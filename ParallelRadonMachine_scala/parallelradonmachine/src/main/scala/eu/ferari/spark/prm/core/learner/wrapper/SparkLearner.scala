package eu.ferari.spark.prm.core.learner.wrapper

import eu.ferari.spark.prm.core.learner.{Learner, Model}
import org.apache.spark.sql.{Dataset, Row}
import scala.reflect._

/**
 * @author Michael Kamp, Fraunhofer IAIS and University of Bonn, michael.kamp@iais.fraunhofer.de
 * Refinement of the general wrapper base class Learner for all machine learning algorithms from Apache Spark
 */
abstract class SparkLearner [M <: Model : ClassTag] extends Learner[M]{
  def fit(data: Dataset[Row]): M
  
  def getNewModelInstance(data: Option[Dataset[Row]] = None): M = {
    classTag[M].runtimeClass.newInstance.asInstanceOf[M]
  }
}