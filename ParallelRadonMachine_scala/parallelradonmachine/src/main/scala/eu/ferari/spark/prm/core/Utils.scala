package eu.ferari.spark.prm.core

import scala.runtime.ScalaRunTime.stringOf
import scala.collection.mutable.ListBuffer
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.SparkContext
import org.apache.spark.sql.{SQLContext, Dataset, Row, Column}
import org.apache.spark.sql.functions._
import org.apache.spark.rdd.RDD.{rddToPairRDDFunctions, rddToOrderedRDDFunctions}
import org.apache.spark.rdd.RDD
import org.apache.spark.HashPartitioner
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.{LongType, StructField, StructType}
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.lit

object Utils {
  
  def transposeRowMatrix(m: RowMatrix): RowMatrix = {
    val transposedRowsRDD = m.rows.zipWithIndex.map{case (row, rowIndex) => rowToTransposedTriplet(row, rowIndex)}
      .flatMap(x => x) // now we have triplets (newRowIndex, (newColIndex, value))
      .groupByKey
      .sortByKey().map(_._2) // sort rows and remove row indexes
      .map(buildRow) // restore order of elements in each row and remove column indexes
    new RowMatrix(transposedRowsRDD)
  }

  protected def rowToTransposedTriplet(row: Vector, rowIndex: Long): Array[(Long, (Long, Double))] = {
    val indexedRow = row.toArray.zipWithIndex
    indexedRow.map{case (value, colIndex) => (colIndex.toLong, (rowIndex, value))}
  }

  protected def buildRow(rowWithIndexes: Iterable[(Long, Double)]): Vector = {
    val resArr = new Array[Double](rowWithIndexes.size)
    rowWithIndexes.foreach{case (index, value) =>
      resArr(index.toInt) = value
    }
    Vectors.dense(resArr)
  }
  
  //bucket is a map from bucket number to a Map from label value to a two-element list. The first element gives the current number 
  //of rows with that label value in the bucket, the second one gives the desired number of rows.    
  def getStratifiedSample(data: Dataset[Row], c: Int, randomSeed: Option[Long] = None): Array[Dataset[Row]] = {
    val sqlContext = data.sqlContext
    import sqlContext.implicits._
    
    //build the bucket
    val labels = data.select("label").distinct.map{row => row.get(0).asInstanceOf[Double].toInt}.collect().toList
    val distinctLabelCounts = data.groupBy("label").agg(count("label"))
    //distinctLabelCounts.show()
    val N = data.count().toDouble
    var fractions = distinctLabelCounts.map(x => x(0).asInstanceOf[Double].toInt -> (x(1).asInstanceOf[Long].toDouble/N)).collect.toMap
    //println(stringOf(fractions))
    var buckets: Map[Int,Map[Int,Array[Int]]] = Map()
    var bucketMap: Map[Int, ListBuffer[Int]] = Map()
    for (i <- ListBuffer.range(0,c)) {
      //println(stringOf((N / c.toDouble) + 1.0)+ "  " + stringOf((((N / c.toDouble) + 1.0) * fractions(labels(1))).toInt + 1))
      var labelMap = labels.map(l=> l -> Array(0, (((N / c.toDouble) + 1.0) * fractions(l)).toInt + 1)).toMap[Int, Array[Int]]
      buckets += i -> labelMap
    }
    for (l <- labels) {
      bucketMap += l -> ListBuffer.range(0,c)
    }
    //create keys based on buckets    
    val labelIdx = data.columns.indexOf("label")    
    //val keyedData = data.rdd.map{row => var label = row(labelIdx); println(stringOf(label)+ "  c=" + stringOf(thecount)); thecount += 1; (getKey(label, c), row)}
    val keyedData = data.rdd.map{row => getKeyValueTuple(row, c, labelIdx, buckets, bucketMap)}.cache()

    //filter the RDDs according to the keys
    val sampleKeys = List.range(0,c)
    val sampleRDDs: List[RDD[Row]] = sampleKeys.map{x => keyedData.filter{case (key, value) => key == x}.map{x => x._2}}
    var retDatasets = Array[Dataset[Row]]()
    for (rdd <- sampleRDDs) { 
      retDatasets = retDatasets :+ sqlContext.createDataFrame(rdd, data.schema)
    }
    return retDatasets
  } 
  
  //bucket is a map from bucket number to a Map from label value to a two-element list. The first element gives the current number 
  //of rows with that label value in the bucket, the second one gives the desired number of rows.    
  def getStratifiedKeys(data: Dataset[Row], c: Int, randomSeed: Option[Long] = None): Dataset[Row] = {
    val sqlContext = data.sqlContext
    import sqlContext.implicits._
    
    //build the bucket
    val labels = data.select("label").distinct.map{row => row.get(0).asInstanceOf[Double].toInt}.collect().toList
    val distinctLabelCounts = data.groupBy("label").agg(count("label"))
    //distinctLabelCounts.show()
    val N = data.count().toDouble
    var fractions = distinctLabelCounts.map(x => x(0).asInstanceOf[Double].toInt -> (x(1).asInstanceOf[Long].toDouble/N)).collect.toMap
    //println(stringOf(fractions))
    var buckets: Map[Int,Map[Int,Array[Int]]] = Map()
    var bucketMap: Map[Int, ListBuffer[Int]] = Map()
    for (i <- ListBuffer.range(0,c)) {
      //println(stringOf((N / c.toDouble) + 1.0)+ "  " + stringOf((((N / c.toDouble) + 1.0) * fractions(labels(1))).toInt + 1))
      var labelMap = labels.map(l=> l -> Array(0, (((N / c.toDouble) + 1.0) * fractions(l)).toInt + 1)).toMap[Int, Array[Int]]
      buckets += i -> labelMap
    }
    for (l <- labels) {
      bucketMap += l -> ListBuffer.range(0,c)
    }
    //add index to the data DF
    val dataIndexed= sqlContext.createDataFrame(
      data.rdd.zipWithIndex.map(ln => Row.fromSeq(Seq(ln._2) ++ ln._1.toSeq)),
      StructType(Array(StructField("idx", LongType, false)) ++ data.schema.fields)
    )
    //create keys based on buckets (this has to happen centralized. Otherwise, the buckets would not be synchronized.   
    val labelColIndexed = dataIndexed.select("idx", "label").map{row => (row.get(0).asInstanceOf[Long].toInt, row.get(1).asInstanceOf[Double].toInt)}.collect()
    var keyMap: scala.collection.mutable.Map[Int, Int] = scala.collection.mutable.Map()    
    for ((idx, label) <- labelColIndexed) {
      keyMap(idx) = getKey(label, c, buckets, bucketMap)
    }           
    //add the keys to the data DataFrame
    val keyForIdx = udf(keyMap(_: Int))    
    val keyedData = dataIndexed.withColumn("key", keyForIdx(dataIndexed("idx"))).drop("idx")
    return keyedData
  }       

  
  def getKeyValueTuple(row: Row, c: Int, labelIdx: Int, buckets: Map[Int,Map[Int,Array[Int]]], bucketMap: Map[Int, ListBuffer[Int]]): Tuple2[Int, Row] = {
    var label = row(labelIdx).asInstanceOf[Double].toInt    
    var key = getKey(label, c, buckets, bucketMap)
    //println(label.toString() + ", " + key.toString())
    return (key, row)
  }
  
  def getKey(label: Int, c: Int, buckets: Map[Int,Map[Int,Array[Int]]], bucketMap: Map[Int, ListBuffer[Int]]): Int = {    
    var bucketNum = -1
    var rand = scala.util.Random
    while (bucketNum < 0) {
      var availableBuckets = bucketMap(label).length
      //println("label: "+stringOf(label)+ " avail. buckets: "+stringOf(availableBuckets))
      bucketNum = bucketMap(label)(rand.nextInt(availableBuckets))
      if (buckets(bucketNum)(label)(0).asInstanceOf[Int] + 1 <= buckets(bucketNum)(label)(1)) {
        buckets(bucketNum)(label)(0) += 1
      }
      else {
        bucketMap(label) -= bucketNum
        bucketNum = -1
      }
    }
    //println(stringOf(buckets))
    //println(stringOf(bucketMap))
    return bucketNum
  }
  
  def dfZipWithIndex( df: Dataset[Row], offset: Int = 1, colName: String = "id", inFront: Boolean = true, maxIndex: Int = -1) : Dataset[Row] = {
    df.sqlContext.createDataFrame(
      df.rdd.zipWithIndex.map(ln => {
          var seq = (if (inFront) Seq(if (maxIndex>0) (ln._2 + offset) % maxIndex else (ln._2 + offset)) else Seq()) ++ ln._1.toSeq ++ (if (inFront) Seq() else Seq(if (maxIndex>0) (ln._2 + offset) % maxIndex else (ln._2 + offset)))          
          Row.fromSeq(seq) 
        }
      ),
      StructType(
        (if (inFront) Array(StructField(colName,LongType,false)) else Array[StructField]()) 
          ++ df.schema.fields ++ 
        (if (inFront) Array[StructField]() else Array(StructField(colName,LongType,false)))
      )
    )
  }
}