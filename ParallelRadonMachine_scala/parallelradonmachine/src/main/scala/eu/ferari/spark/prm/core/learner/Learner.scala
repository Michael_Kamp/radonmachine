package eu.ferari.spark.prm.core.learner

import org.apache.spark.sql.{Dataset, Row, SparkSession}
import eu.ferari.spark.prm.core.learner.wrapper._
import eu.ferari.spark.prm.prmannotations.WrapsLearner
import java.lang.annotation._
import reflect.runtime.universe._
import java.util.ServiceLoader
import org.reflections._
import scala.reflect._
import scala.collection.JavaConverters._
import scala.collection.JavaConversions._
import scala.reflect.runtime.{ universe => ru }

/**
 * @author Michael Kamp, Fraunhofer IAIS and University of Bonn, michael.kamp@iais.fraunhofer.de
 * Base class for learners that can be parallelized by the Parallel Radon Machine. Every wrapper needs to implement this class.
 * 
 */
abstract class Learner[M <: Model] extends Serializable {
  protected val config: scala.collection.mutable.Map[String, Any]
  
  def fit(data: Dataset[Row]): Model
  def getModelClass()(implicit m: Manifest[M]): Class[M] = m.erasure.asInstanceOf[Class[M]]  
  def getNewModelInstance(data: Option[Dataset[Row]] = None): Model
    
  def setConfig(newConfig: scala.collection.mutable.Map[String, Any]): Unit = {
    for (key <- config.keys) {
      if (newConfig.contains(key)) {
        config.update(key, newConfig(key))
      }
    }
    newConfig.foreach{ case (key,value) => if (!config.keySet.contains(key)) { println("WARNING: unknown parameter '"+key+"'") } }    
  }  
  def getConfig(): scala.collection.mutable.Map[String, Any] = {
    return config
  }    
  def getConfigString(): String = {
    var strOut = ""
    config.foreach { case(key, value) => strOut += key + "=" + value.toString() + ";" }
    return strOut
  }
  
  override def toString(): String = {
    var stName = this.getClass.toString()
    stName = stName.replace("eu.ferari.spark.prm.core", "").replace(".learner.wrapper", "").replace(".sparkwrapper.", "").replace(".wekawrapper.", "").replace("class ", "")
    stName = stName.replace(":", "").replace("/", "")
    return stName
  }
}

object Learner {    
  def apply(algorithm : ClassTag[_]) : Option[Learner[_ <: Model]] = {
    val reflections = new Reflections("eu.ferari.spark.prm.core.learner.wrapper");    
    var wrappers = reflections.getTypesAnnotatedWith(classOf[WrapsLearner]);
    for (c <- wrappers) {
      val annotations = getType(ClassTag(c)).typeSymbol.asClass.annotations.map { a =>
        val annotationName = a.tpe.typeSymbol.name.toString
        val annotationArgs = a.javaArgs.map { case (name, value) =>
          name.toString -> value
        }  
        annotationName -> annotationArgs
      }
      val optAnnotationClassName = annotations.find((p) => p._1 == "WrapsLearner")
      //val annotations = getType(ClassTag(c)).typeSymbol.asClass.annotations(0)
      //val optAnnotationClassName = annotations.javaArgs.find((t) => t._1.toString() == "name")
      if (optAnnotationClassName.isDefined) {
        val annotationClassName = optAnnotationClassName.get._2.find(p => p._1.toString() == "name").get._2
        val strClassName = annotationClassName.toString().replace("classOf[","").replace("]","")
        if (ClassTag(Class.forName(strClassName)) == algorithm) {
          var instance = instantiate(ClassTag(c))
          return Some(instance)
        }
      }
    }
    return None
  }    
  
  def getWrappedLearners() : Array[ClassTag[_]] =  {
    var ret = new  Array[ClassTag[_]](0)
    val reflections = new Reflections("eu.ferari.spark.prm.core.learner.wrapper");    
    var wrappers = reflections.getTypesAnnotatedWith(classOf[WrapsLearner]);
    for (c <- wrappers) {
      val annotations = getType(ClassTag(c)).typeSymbol.asClass.annotations.map { a =>
        val annotationName = a.tpe.typeSymbol.name.toString
        val annotationArgs = a.javaArgs.map { case (name, value) =>
          name.toString -> value
        }  
        annotationName -> annotationArgs
      }
      val optAnnotationClassName = annotations.find((p) => p._1 == "WrapsLearner")
      if (optAnnotationClassName.isDefined) {
        val annotationClassName = optAnnotationClassName.get._2.find(p => p._1.toString() == "name").get._2
        val strClassName = annotationClassName.toString().replace("classOf[","").replace("]","")
        var learnerClass = ClassTag(Class.forName(strClassName))
        ret = ret :+ learnerClass
      }
    }
    return ret
  }
  
  def getType(clazz: ClassTag[_]) : ru.Type = {
    val runtimeMirror =  ru.runtimeMirror(clazz.runtimeClass.getClassLoader)
    return runtimeMirror.classSymbol(clazz.runtimeClass).toType    
  }
  
  def instantiate(clazz : ClassTag[_]): Learner[Model] = {    
    val runtimeMirror =  ru.runtimeMirror(clazz.runtimeClass.getClassLoader)
    val instance = clazz.runtimeClass.newInstance
    return instance.asInstanceOf[Learner[Model]]
  }
}