package eu.ferari.spark.prm.core.learner.wrapper

import eu.ferari.spark.prm.core.learner.{Learner, Model}
import org.apache.spark.sql.{Dataset, Row}
import scala.reflect._

/**
 * @author Michael Kamp, Fraunhofer IAIS and University of Bonn, michael.kamp@iais.fraunhofer.de
 * Refinement of the general wrapper base class Learner for all serial ("normal") machine learning algorithms. 
 */
abstract class SerialLearner[M <: Model : ClassTag] extends Learner[M]{
  
  implicit def kryoEncoder[A](implicit ct: ClassTag[A]) = org.apache.spark.sql.Encoders.kryo[A](ct)
  
  def fit(data: Dataset[Row]): M = {
    val labelIdx = data.columns.indexOf("label")
    if (data.rdd.getNumPartitions > 1) { //enforce a single partition
      data.repartition(1)      
    }
    //transform dataset to single Row containing all former Rows in a list
    var reducedData = data.sparkSession.createDataset[Row](List(data.reduce((r1, r2) => {  
                              var newRow: Row = if (r1(0).isInstanceOf[List[_]]
                                                    && r1(0).asInstanceOf[List[_]](0).isInstanceOf[Row]) { r1 } 
                                                else { Row(List(r1)) }
                              if (r2(0).isInstanceOf[List[_]] 
                                  && r2(0).asInstanceOf[List[_]](0).isInstanceOf[Row]) {
                                var list = newRow(0).asInstanceOf[List[Row]] ++ r2(0).asInstanceOf[List[Row]]
                                newRow = Row(list)
                              }
                              else { 
                                var list = newRow(0).asInstanceOf[List[Row]] ++ List(r2)
                                newRow= Row(list)
                              }
                              newRow
                            })))
    //var reducedData = data.sparkSession.createDataset[Row](List(Row(data.collect().toList)))
    var modelParams = reducedData.map(applyFitToRowOfListOfRows(labelIdx)).collect
    //var modelParams = data.mapPartitions(applyFit(labelIdx)).collect
    if (modelParams.length > 1) {
      println("FATAL ERROR: Number of recieved models > 1: "+modelParams.length.toString)
    }    
    var model = this.getNewModelInstance(Option(data)).asInstanceOf[M]
    //model.setParameters(modelParams(0))
    model.setParameters(modelParams(0)(0).asInstanceOf[Array[Double]])
    return model//since there is just a single partition, the number of models should be 1
  }
  
  def applyFit(labelIdx: Int): Iterator[Row] => Iterator[Array[Double]] = {
    def _fit(iter: Iterator[Row]): Iterator[Array[Double]] = {
      return Array(fit(iter.toList, labelIdx)).toIterator
    }
    return _fit
  }
  
  def applyFitToRowOfListOfRows(labelIdx: Int): Row => Row = {
    def _fit(r: Row): Row = {      
      return Row(fit(r(0).asInstanceOf[List[Row]], labelIdx))
    }
    return _fit
  }
  
  def fit(data: List[Row], labelIdx: Int): Array[Double]
}