package eu.ferari.spark.prm.experiments.mlj.classification

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import eu.ferari.spark.prm.core.learner._
import eu.ferari.spark.prm.core.ParallelRadonMachine
import eu.ferari.spark.prm.experiments.{Experiment, ExperimentDefinition}
import eu.ferari.spark.prm.experiments.datautils.{DataUtil, HdfsDataUtil}

object RadonSplitVsMapPartition extends ExperimentDefinition{
  var name = "MLJ_Classification_RadonSplitVsMapPartition"
  var clusterMode = true 
  var expBasePath = "/home/IAIS/mkamp/scala/PRM/experiments/"
  
  def run(sqlContext: SQLContext): Unit = {   
    var datasets = HdfsDataUtil.datasets("classification").keys.toArray //DataUtil.datasets("classification").keys.toArray //the keys contain the dataset names (the values contain the path to the files)
    
    datasets.foreach{ dataset =>
      var sparklearners: Array[Learner[Model]] = Learner.getWrappedLearners().map(l => Learner(l).get.asInstanceOf[Learner[Model]]).filter(l => (l.getClass.toString().contains("Logistic") || l.getClass.toString().contains("SVM")|| l.getClass.toString().contains("SparkMLLinearRegression") || l.getClass.toString().contains("Perceptron") || l.getClass.toString().contains("WekaLinearRegression")))
      
      var radonMachinesMapPartition = sparklearners.map(l => {
           var prm = new ParallelRadonMachine(l, "map")
           prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> -1, "minSampleSize" -> 50))
           prm.asInstanceOf[Learner[Model]]
        }).toArray
      var radonMachinesSplit = sparklearners.map(l => {
           var prm = new ParallelRadonMachine(l, "split")
           prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> -1, "minSampleSize" -> 50, "useMapPartition" -> false))
           prm.asInstanceOf[Learner[Model]]
        }).toArray
      var learners = radonMachinesSplit ++ radonMachinesMapPartition
      var metrics = Array(AverageZeroOneLoss, AverageHingeLoss, ACC, AUC)
      
      
      val nFolds = 10
      val exp = new Experiment(name+"_"+dataset.replace(".dat",""), clusterMode, expBasePath)
      exp.run(sqlContext, Array(dataset), learners, metrics, nFolds)     
    }
  }
}