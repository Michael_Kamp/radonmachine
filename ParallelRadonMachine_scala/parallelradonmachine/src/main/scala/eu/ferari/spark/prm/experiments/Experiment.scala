package eu.ferari.spark.prm.experiments

import eu.ferari.spark.prm.core.learner.{Learner, Model, Metric}
import eu.ferari.spark.prm.experiments.{ExperimentLogger => ELogger}
import eu.ferari.spark.prm.experiments.datautils.{DataUtil, HdfsDataUtil}
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.sql.{SQLContext, Dataset, Row}
import java.util.Date
import java.text.SimpleDateFormat
import eu.ferari.spark.prm.core.Logger

/**
 * runBaseLearnerExpOnDataset: data, c => prints predictions and durations to file
 * runCentralizedBaseLEarnerExpOnDataset = runBaseLearnerExpOnDataset(data, c=1)
 * runPRMExpOnDataset: data, c, h => prints predictions and durations to file
 */
class Experiment(name: String, clusterMode: Boolean = false, expBasePath: String = "../experiments/") {
  val expStartTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date(System.currentTimeMillis()))
  val expPath = expBasePath + name + "/" + expStartTime + "/"
  val expLog = new ELogger(expPath)
  
  def run(sqlContext: SQLContext, datasets: Array[String], learners: Array[Learner[Model]], metrics: Array[Metric], nfolds: Int, randomSeed: Option[Long] = None): Unit = {    
    for (datasetName <- datasets) {
      println("Starting experiment with dataset "+datasetName)
      Logger.log.warn("Starting experiment with dataset "+datasetName)
      val data = if (clusterMode) HdfsDataUtil.getDataset(datasetName, sqlContext) else DataUtil.getDataset(datasetName, sqlContext)
      val schema = data.schema
      val splits = MLUtils.kFold(data.rdd, nfolds, randomSeed.getOrElse(System.nanoTime()).toInt)
      var fold = 0
      splits.zipWithIndex.foreach { case ((training, validation), splitIndex) =>
        val trainingDataset = sqlContext.createDataFrame(training, schema).cache()
        val validationDataset = sqlContext.createDataFrame(validation, schema).cache()
        for (learner <- learners) {
          println("Training fold "+fold.toString()+" for learner "+learner.toString())
          
          expLog.writeLearnerConfig(learner, fold, datasetName)
          
          val startFit = System.nanoTime
          var model: Model = fitSafely(trainingDataset, learner)
          val endFit = System.nanoTime
          
          val startPred = System.nanoTime
          var predVals: Array[Double] =  predictSafely(validationDataset, model, validationDataset.count.toInt)
          val endPred = System.nanoTime
          
          //the following should be handable even for very large dataset sizes, since we only need to collect one double
          //column. Thus, even for 1,000,000,000 instances, we would have an array of less than 8GB, which should fit into main memory.
          var trueVals = validationDataset.select("label").collect().map(x=>x.getAs[Double]("label")).toArray
          
          expLog.writePredictionLog(trueVals, predVals, datasetName, learner, fold)
          
          var errors = new Array[Double](metrics.length)
          for (i <- List.range(0, metrics.length)) {
            errors(i) = metrics(i)(trueVals, predVals)
          }
          
          expLog.writeResults(endFit - startFit, endPred - startPred, errors, metrics.map(m => m.toString()), learner, datasetName, fold)
        }
        fold += 1
      }      
    }
  }
  
  def fitSafely(data: Dataset[Row], learner: Learner[Model]): Model = {
    try {
      learner.fit(data)
    } catch {
      case e: Exception => { 
        println("Exception in fit: "+e.toString())
        try {
          learner.getNewModelInstance()
        } catch {
          case e: Exception => {
            println("Exception in getting new model instance: "+e.toString())
            learner.getNewModelInstance(Option(data))
          }
        }
      }
    }
  }
  
  def predictSafely(data: Dataset[Row], model: Model, N:Int = -1): Array[Double] = {
    try {
      model.predict(data)
    } catch {
    case e: Exception => { 
        println("Exception in fit: "+e.toString())
        data.show()
        var tN = N
        if (tN < 0) { 
          tN = data.count().toInt
        }
        Array.fill[Double](tN)(0.0)
      }
    }          
  }
}