package eu.ferari.spark.prm.experiments.datautils

import java.io.File
import org.apache.spark.sql.{SQLContext, Dataset, Row}
import org.apache.spark.ml.feature.StandardScaler
import org.apache.hadoop.fs.{FileSystem, FileStatus, Path}
import org.apache.hadoop.conf.Configuration
import eu.ferari.spark.prm.core.Logger

abstract class DataUtilBase {
  val taskTypes = List("classification", "regression", "multi-class")  
  var datasets: Map[String, scala.collection.mutable.Map[String,String]] = taskTypes.map{t => t -> scala.collection.mutable.Map[String, String]()}.toMap[String, scala.collection.mutable.Map[String,String]]    
  val dataFolder: String
  def getListOfFiles(dir: String):List[(String, String)]
  
  def fillDatasets() {
    for (taskType <- taskTypes) {
      for ((name, path) <- getListOfFiles(dataFolder+taskType+"/")) {
        datasets(taskType) += name -> path
      }
    }
    Logger.log.debug(datasets)
  }
  
  
  def getDataset(name: String, sqlContext: SQLContext) : Dataset[Row] = {
    var filepath = ""
    var taskType = taskTypes(0)
    var i = 0
    println(name)
    while(i < taskTypes.length & filepath == "") {
      taskType = taskTypes(i)
      println(taskType)
      if (datasets(taskType).contains(name)) {
        filepath = datasets(taskType)(name)        
      }
      else if (datasets(taskType).contains(name+".dat")) {
        filepath = datasets(taskType)(name+".dat")        
      }
      i += 1
    }
    println("\nReading dataset "+name+" as "+taskType+"-task from location "+filepath+" ...")
    Logger.log.warn("\nReading dataset "+name+" as "+taskType+"-task from location "+filepath+" ...")
    var dataset = sqlContext.read.format("libsvm").load(filepath)
    println("done.")
    Logger.log.warn("done.\n")
    return dataset
  }
  
  /**
   * Performs whitening, i.e., transforms data to zero mean and unit variance.
   */
  def normalizeDataset(data: Dataset[Row]): Dataset[Row] = {
    val scaler = new StandardScaler()
      .setInputCol("features")
      .setOutputCol("scaledFeatures")
      .setWithStd(true)
      .setWithMean(true)
    val scalerModel = scaler.fit(data)
    return scalerModel.transform(data)
  }
}

object DataUtil extends DataUtilBase {    
  val dataFolder = "../data/"//getClass.getResource("").getPath()+"../../../../../../../../../../data/" 
  val path = new Path(dataFolder)
  fillDatasets()
  
  def getListOfFiles(dir: String):List[(String, String)] = {
    val d = new File(dir)
    var files = List[File]()
    if (d.exists && d.isDirectory) {
      files = d.listFiles.filter(_.isFile).toList
    } else {
      files = List[File]()
    }
    var retValues:List[(String, String)] = files.map{file => (file.getName().toString().substring(0,file.getName().toString().indexOf(".")), file.toString())}
    return retValues
  }
}

object HdfsDataUtil extends DataUtilBase {  
  val dataFolder = "hdfs://str22:9000/user/mkamp/data/"  
  fillDatasets()
  
  def getListOfFiles(dir: String):List[(String, String)] = {
    var fileList = List[(String, String)]();
    
    val fs = FileSystem.get(new Configuration())
    var status = fs.listStatus(new Path(dir))
    for (fileStat <- status) {
        if (fileStat.isDir() ) {
            fileList = fileList ++ getListOfFiles(fileStat.getPath.toString);
        } else {
          if (fileStat.getPath.getName.contains(".dat")) {
            fileList = fileList ++ List((fileStat.getPath.getName, fileStat.getPath.toString))
          }
        }
    }    
    return fileList
  }
}