package eu.ferari.spark.prm.core.learner

abstract class Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double
  
  override def toString() : String = {
    var stName = this.getClass.toString()
    stName = stName.replace("eu.ferari.spark.prm.core", "").replace(".learner.", "").replace("class ", "")
    stName = stName.replace(":", "").replace("/", "").replace("\\", "").replace("$", "")
    return stName
  }
  
  /**
   * For binary classification only
   */
  def getTPRandFPRfromConfustionMatrix(confusionMatrix: Array[Int]) : Array[Double] = {
    var tp = confusionMatrix(0)
    var fp = confusionMatrix(1)
    var tn = confusionMatrix(2)
    var fn = confusionMatrix(3)
    var TPR = tp.toDouble / (tp+fn).toDouble
    var FPR = fp.toDouble / (fp+tn).toDouble
    return Array(TPR, FPR)
  }
    
  /**
   * For binary classification only
   */
  def getConfusionMatrix(trueVals: Array[Double], predVals: Array[Double], threshold: Double = 0.0) : Array[Int] = {
    //binarize the input
    val labels = trueVals.distinct
    if (labels.length != 2) {
      throw new IllegalArgumentException("Labels are not binary, but have "+labels.length.toString()+" classes.");
    }
    var preds = predVals
    if (labels.length == 2 & predVals.distinct.length > 2) {
      val lgz = if (labels(0) >  0) labels(0) else labels(1)
      val lsz = if (labels(0) <= 0) labels(0) else labels(1)
      preds = predVals.map(x => if (x <= threshold)  lsz  else  lgz)
    }
    var tp = 0
    var fp = 0
    var tn = 0
    var fn = 0
    for (i <- List.range(0,preds.length)) {
      if (trueVals(i).toInt == 1) {
        if (preds(i) == 1) { tp += 1 }
        else {               fn += 1 }        
      }
      else {
        if (preds(i) == 1) { fp += 1 }
        else {               tn += 1 }
      }
    }
    return Array(tp, fp, tn, fn)
  }
}

object ZeroOneLoss extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    val labels = trueVals.distinct
    var preds = predVals
    if (labels.length == 2 & predVals.distinct.length > 2) {
      val lgz = if (labels(0) >  0) labels(0) else labels(1)
      val lsz = if (labels(0) <= 0) labels(0) else labels(1)
      preds = predVals.map(x => if (x <= 0.0)  lsz  else  lgz)
    }
    var error = 0.0
    for (i <- List.range(0, preds.length)) {
      if (preds(i) != trueVals(i)) {
        error += 1.0
      }
    }
    return error
  }
}

object AverageZeroOneLoss extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    val labels = trueVals.distinct
    var preds = predVals
    if (labels.length == 2 & predVals.distinct.length > 2) {
      val lgz = if (labels(0) >  0) labels(0) else labels(1)
      val lsz = if (labels(0) <= 0) labels(0) else labels(1)
      preds = predVals.map(x => if (x <= 0.0)  lsz  else  lgz)
    }
    var error = 0.0
    for (i <- List.range(0, preds.length)) {
      if (preds(i) != trueVals(i)) {
        error += 1.0
      }
    }
    error /= preds.length.toDouble
    return error
  }
}

object HingeLoss extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    var error = 0.0
    val trueValsBin = trueVals.map{x => if (x==0) -1.0 else x} //if the binary class is 0,1 encoded, transform it to -1,+1
    for (i <- List.range(0, predVals.length)) {
      var l = 1.0 - predVals(i)*trueValsBin(i)
      error += (if (l > 0.0) l else 0.0)
    }
    return error
  }
}

object AverageHingeLoss extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    return HingeLoss(trueVals, predVals) / predVals.length.toDouble
  }
}

object ACC extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    var confusionMatrix = getConfusionMatrix(trueVals, predVals)
    var tp = confusionMatrix(0)
    var fp = confusionMatrix(1)
    var tn = confusionMatrix(2)
    var fn = confusionMatrix(3)
    return (tp + tn).toDouble / (tp + tn +fp + fn).toDouble
  }
}

/**
 * Area under the RO-Curve
 */
object AUC extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    //first, binarize the predictions    
    var thresholds = predVals.clone()
    scala.util.Sorting.quickSort(thresholds) //sorts in ascending order
    thresholds = thresholds.reverse.distinct
    var FPRs = List.fill(thresholds.length)(1.0).toArray
    var TPRs = List.fill(thresholds.length)(1.0).toArray
    for (i <- List.range(1,thresholds.length)) {
      var tprfpr = getTPRandFPRfromConfustionMatrix(getConfusionMatrix(trueVals, predVals, thresholds(i)))
      TPRs(i-1) = tprfpr(0) //TPR
      FPRs(i-1) = tprfpr(1) //FPR      
    }    
    var auc = 0.0
    for (i <- List.range(0, TPRs.length -1)) {
      if (FPRs(i+1) > FPRs(i)) {
        auc  += ((TPRs(i+1) + TPRs(i))/2.0) * (FPRs(i+1) - FPRs(i))
      }
    }
    return auc
  }
}

/**
 * Mean Relative Error
 */
object MRE extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    var error = 0.0
    for (i <- List.range(0, predVals.length)) {
       error += (scala.math.abs(predVals(i) - trueVals(i))) / (if (trueVals(i) != 0.0) scala.math.abs(trueVals(i)) else 1.0)
    }
    error /= predVals.length.toDouble
    return error
  }
}

/**
 * Relative Change and Difference
 */
object RCD extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    var error = 0.0
    for (i <- List.range(0, predVals.length)) {
      var x = trueVals(i)
      var y = predVals(i)
      error += (scala.math.abs(x - y)) / (scala.math.max(scala.math.abs(x), scala.math.abs(y)))
    }
    error /= predVals.length.toDouble
    return error
  }
}

/**
 * Mean Squared Error
 */
object MSE extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {
    var error = 0.0
    for (i <- List.range(0, predVals.length)) {
      if (predVals(i) != trueVals(i)) {
        error += scala.math.pow((predVals(i) - trueVals(i)), 2)
      }
    }
    error /= predVals.length.toDouble
    return error
  }
}

/**
 * Root Mean Squared Error
 */
object RMSE extends Metric {
  def apply(trueVals: Array[Double], predVals: Array[Double]) : Double = {    
    return scala.math.sqrt(MSE(trueVals, predVals))
  }
}