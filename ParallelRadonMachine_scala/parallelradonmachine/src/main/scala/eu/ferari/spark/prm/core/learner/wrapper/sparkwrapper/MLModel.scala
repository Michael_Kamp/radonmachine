package eu.ferari.spark.prm.core.learner.wrapper.sparkwrapper

import scala.reflect.ClassTag
import scala.reflect._
import eu.ferari.spark.prm.core.learner.Model
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.sql.Row
import org.apache.spark.ml.linalg.{Vector, Vectors, Matrix, Matrices}
import org.apache.spark.ml.{Pipeline, PipelineModel, PredictionModel}
import org.apache.spark.ml.classification.{LogisticRegressionModel, MultilayerPerceptronClassificationModel}
import org.apache.spark.ml.regression.{LinearRegressionModel}
import org.apache.spark.ml.util.Identifiable

/* Spark ML Models (Dataset[Row]-Interface)*/
// <: PredictionModel[Vector, _]
abstract class SparkMLModel[M <: PredictionModel[_, _]] extends Model{
  var model: M = createDummyModel()  
  val sparkidentifier : String   
  
  protected def createDummyModel() : M
  
  def setModel(model : M) {
    this.model = model
  }
  def predict(data: Dataset[Row]): Array[Double] = {
    import data.sqlContext.implicits._
    return model.transform(data).select("prediction").map{ case Row(prediction: Double) => prediction }.collect()
  }
}


class SparkMlLogisticRegressionModel extends SparkMLModel[LogisticRegressionModel] {
  override val sparkidentifier = "logreg"
  
  protected def createDummyModel(): LogisticRegressionModel = {
    var coefficients = Vectors.dense(Array(0.0))
    var intercept = 0.0
    val ctor = classOf[LogisticRegressionModel].getDeclaredConstructor(classOf[String], classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    return ctor.newInstance(Identifiable.randomUID(sparkidentifier), coefficients, new java.lang.Double(intercept))
  }
  
  def getParameters(): Array[Double] = {
    var parameters : Array[Double] = this.model.coefficients.toArray
    parameters = parameters :+ this.model.intercept //TODO: check if that really works this way
    return parameters
  }   
  
  def setParameters(parameters: Array[Double]) {
    this.parameters = parameters
    var coefficients = Vectors.dense(parameters.slice(0, parameters.length - 1))
    var intercept = parameters(parameters.length-1)
    val ctor = model.getClass.getDeclaredConstructor(classOf[String], classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    this.model = ctor.newInstance(Identifiable.randomUID(sparkidentifier), coefficients, new java.lang.Double(intercept))
  }
}

class SparkMlLinearRegressionModel extends SparkMLModel[LinearRegressionModel] {
  override val sparkidentifier = "linreg"
  
  protected def createDummyModel(): LinearRegressionModel = {
    var coefficients = Vectors.dense(Array(0.0))
    var intercept = 0.0
    val ctor = classOf[LinearRegressionModel].getDeclaredConstructor(classOf[String], classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    return ctor.newInstance(Identifiable.randomUID(sparkidentifier), coefficients, new java.lang.Double(intercept))
  }
  
  def getParameters(): Array[Double] = {
    var parameters : Array[Double] = this.model.coefficients.toArray
    parameters = parameters :+ this.model.intercept 
    return parameters
  }   
  
  def setParameters(parameters: Array[Double]) {
    this.parameters = parameters
    var coefficients = Vectors.dense(parameters.slice(0, parameters.length - 1))
    var intercept = parameters(parameters.length-1)
    val ctor = model.getClass.getDeclaredConstructor(classOf[String], classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    this.model = ctor.newInstance(Identifiable.randomUID(sparkidentifier), coefficients, new java.lang.Double(intercept))
  }
}

/**
 * This class only allows to adapt the weights of the MLP, not the Layers
 */
class SparkMlMultilayerPerceptronClassificationModel extends SparkMLModel[MultilayerPerceptronClassificationModel] {
  override val sparkidentifier = "mlpclassifier"
  
  protected def createDummyModel(): MultilayerPerceptronClassificationModel = {
    val ctor = classOf[MultilayerPerceptronClassificationModel].getDeclaredConstructor(classOf[String], classOf[Array[Int]], classOf[Vector])
    ctor.setAccessible(true)
    var coefficients = Vectors.dense(Array(0.0, 0.0))
    var layers = Array[Int](1,2)
    return ctor.newInstance(Identifiable.randomUID(sparkidentifier), layers, coefficients)
  }
  
  def getParameters(): Array[Double] = {
    var parameters : Array[Double] = this.model.weights.toArray
    return parameters
  }   
  
  def setParameters(parameters: Array[Double]) {
    this.parameters = parameters
    var weights = Vectors.dense(parameters)
    val ctor = model.getClass.getDeclaredConstructor(classOf[String], classOf[Array[Int]], classOf[Vector])    
    ctor.setAccessible(true)
    var layers = this.model.layers
    this.model = ctor.newInstance(Identifiable.randomUID(sparkidentifier), layers, weights)
  }
  
  def setParameters(parameters: Array[Double], layers: Array[Int]) {
    this.parameters = parameters
    var weights = Vectors.dense(parameters)
    val ctor = model.getClass.getDeclaredConstructor(classOf[String], classOf[Array[Int]], classOf[Vector])    
    ctor.setAccessible(true)
    this.model = ctor.newInstance(Identifiable.randomUID(sparkidentifier), layers, weights)
  }
}