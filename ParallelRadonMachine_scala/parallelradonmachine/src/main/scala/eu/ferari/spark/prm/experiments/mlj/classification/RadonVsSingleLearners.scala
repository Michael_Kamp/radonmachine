package eu.ferari.spark.prm.experiments.mlj.classification

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import eu.ferari.spark.prm.core.learner._
import eu.ferari.spark.prm.core.ParallelRadonMachine
import eu.ferari.spark.prm.experiments.{Experiment, ExperimentDefinition}
import eu.ferari.spark.prm.experiments.datautils.{DataUtil, HdfsDataUtil}

object RadonVsSingleLearners extends ExperimentDefinition{
  var name = "MLJ_Classification_RadonVsSingleLearners"
  var clusterMode = true 
  var expBasePath = "/home/IAIS/mkamp/scala/PRM/experiments/"
  
  def run(sqlContext: SQLContext): Unit = {   
    var datasets = HdfsDataUtil.datasets("classification").keys.toArray //DataUtil.datasets("classification").keys.toArray //the keys contain the dataset names (the values contain the path to the files)
    //datasets = datasets.filterNot { p => p.contains("skin_segmentation") || p.contains("Stagger1") }
    
    datasets.foreach{ dataset =>
      var singleLearners: Array[Learner[Model]] = Learner.getWrappedLearners().map(l => Learner(l).get.asInstanceOf[Learner[Model]]).filter(l => (
          l.getClass.toString().contains("WekaLogisticRegression") || 
          l.getClass.toString().contains("WekaMLP") || 
          l.getClass.toString().contains("WekaSGD")))
      
      var radonMachines = singleLearners.map{l => 
          {
            var prm = new ParallelRadonMachine(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> -1, "minSampleSize" -> 100))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var radonMachinesh1 = singleLearners.map{l => 
          {
            var prm = new ParallelRadonMachine(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> 1, "minSampleSize" -> 100))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var learners = singleLearners ++ radonMachines ++ radonMachinesh1
      var metrics = Array(AverageZeroOneLoss, AverageHingeLoss, ACC, AUC)
      
      
      val nFolds = 10
      val exp = new Experiment(name, clusterMode, expBasePath)
      exp.run(sqlContext, datasets, learners, metrics, nFolds)   
    }
  }
}