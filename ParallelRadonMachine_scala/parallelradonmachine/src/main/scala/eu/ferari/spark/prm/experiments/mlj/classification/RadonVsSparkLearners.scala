package eu.ferari.spark.prm.experiments.mlj.classification

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import eu.ferari.spark.prm.core.learner._
import eu.ferari.spark.prm.core.ParallelRadonMachine
import eu.ferari.spark.prm.experiments.{Experiment, ExperimentDefinition}
import eu.ferari.spark.prm.experiments.datautils.{DataUtil, HdfsDataUtil}

object RadonVsSparkLearners extends ExperimentDefinition{
  var name = "MLJ_Classification_RadonVsSingleVsSpark"
  var clusterMode = true 
  var expBasePath = "/home/IAIS/mkamp/scala/PRM/experiments/"
  
  def run(sqlContext: SQLContext): Unit = {   
    var datasets = HdfsDataUtil.datasets("classification").keys.toArray.filter(_.contains("CASP")) //DataUtil.datasets("classification").keys.toArray //the keys contain the dataset names (the values contain the path to the files)
    
    datasets.foreach{ dataset =>
      var sparklearners: Array[Learner[Model]] = Learner.getWrappedLearners().map(l => Learner(l).get.asInstanceOf[Learner[Model]]).filter(l => (
              l.getClass.toString().contains("SparkMLMultilayerPerceptronClassifier") || 
              l.getClass.toString().contains("SparkMLLogisticRegression")|| 
              l.getClass.toString().contains("SparkMLLibSVMWithSGD")|| 
              l.getClass.toString().contains("SparkMLLibLogisticRegressionWithSGD")|| 
              l.getClass.toString().contains("SparkMLLibLogisticRegressionWithLBFGS")))
      
      var radonMachines = sparklearners.map{l => 
          {
            var prm = new ParallelRadonMachine(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> -1, "minSampleSize" -> 100, "useMapPartition" -> false))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var radonMachinesh1 = sparklearners.map{l => 
          {
            var prm = new ParallelRadonMachine(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> 1, "minSampleSize" -> 100, "useMapPartition" -> false))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var learners = sparklearners ++ radonMachines ++ radonMachinesh1
      var metrics = Array(AverageZeroOneLoss, AverageHingeLoss, ACC, AUC)
      
      
      val nFolds = 10
      val exp = new Experiment(name+"_"+dataset.replace(".dat",""), clusterMode, expBasePath)
      exp.run(sqlContext, Array(dataset), learners, metrics, nFolds) 
    }
  }
}