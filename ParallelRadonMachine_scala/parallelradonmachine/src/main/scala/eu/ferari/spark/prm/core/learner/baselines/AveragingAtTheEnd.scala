package eu.ferari.spark.prm.core.learner.baselines

import eu.ferari.spark.prm.core.learner.{Learner, Model, EmpyModel}
import org.apache.spark.SparkContext
import org.apache.spark.sql.{Dataset, Row, SQLContext}
import eu.ferari.spark.prm.core.radonpoint.RadonPoint
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.types.{DoubleType, StructType, StructField}
import org.apache.spark.sql.Row
import collection.mutable.HashMap
import eu.ferari.spark.prm.core.Utils.getStratifiedSample

class AveragingAtTheEnd (val baseLearner: Learner[_ <: Model]) extends Learner {
  var model: Model = new EmpyModel()
  var durations = HashMap[String, Long] ("DataSplit" -> 0, "ModelFit" -> 0, "RadonCalc" -> 0, "FitTotal" -> 0, "c" -> 1)
  protected val config = scala.collection.mutable.Map[String, Any]("h" -> 1, "c" -> 2, "minSampleSize" -> 100, "minSampleSizeForRadonNumberCalculation" -> 100, "stratifiedSample" -> false)
  
  def calcAverageModel(S: Array[Array[Double]]): Array[Double] = {
    var sum = S.map(breeze.linalg.Vector(_)).reduce(_ + _)
    var fact = (1.0 / S.length.toDouble)
    var avg = sum.toArray.map(x => x * fact) 
    return avg
  }
  
  def fit(data: Dataset[Row]): Model = {
    var h = config("h").asInstanceOf[Int]
    var c = config("c").asInstanceOf[Int]
    if (h>0) {
      val N = data.count()
      val minSampleSizeForRadonNumberCalculation = config("minSampleSizeForRadonNumberCalculation").asInstanceOf[Int]
      val r = RadonPoint.getRadonNumber(data, baseLearner, minSampleSizeForRadonNumberCalculation.toFloat / N.toFloat)
      c = math.pow(r, h).toInt
      while (N/c < 10) {
        h = h - 1
        c = math.pow(r, h).toInt
      }
    }
    return fit(data, c, config("stratifiedSample").asInstanceOf[Boolean])
  }
  
  def getNewModelInstance(data: Option[Dataset[Row]] = None): Model = {
    return baseLearner.getNewModelInstance(data)
  }
  
  def fit(data: Dataset[Row], c: Int, stratifiedSample: Boolean = true, randomSeed: Option[Long] = None) : Model = {    
    val sqlContext = data.sqlContext
    val startFit = System.nanoTime
    val N = data.count()        
    val startSplit = System.nanoTime
    val splittedData = if (!stratifiedSample) data.randomSplit(Array.fill(c)(1), randomSeed.getOrElse(System.nanoTime())) else getStratifiedSample(data, c, randomSeed)
    val endSplit = System.nanoTime
    val durationSplit = endSplit - startSplit
    
    model = baseLearner.fit(splittedData(0))
    
    val startFitOnly = System.nanoTime
    var S = splittedData.map { case subset => {
        //val baseLearner = baseLearnerClass.newInstance()
        val locmodel = baseLearner.fit(subset)
        locmodel.getParameters()
      }      
    }
    val endFitOnly = System.nanoTime    
    val durationFitOnly = endFitOnly - startFitOnly
    
    val startRadon = System.nanoTime
    
    var parameters = calcAverageModel(S)
    
    val endRadon = System.nanoTime    
    val durationRadon = endRadon - startRadon
    
    model.setParameters(parameters)    
    
    val endFit = System.nanoTime
    val durationFit = endFit - startFit
    
    durations = HashMap("DataSplit" -> durationSplit, "ModelFit" -> durationFitOnly, "AvgCalc" -> durationRadon, "FitTotal" -> durationFit, "c" -> c)
    return model
  }
  
  def predict(data: Dataset[Row]): Dataset[Row] = {    
    val sqlContext = data.sqlContext
    val predArray = model.predict(data)    
    val predictions = sqlContext.sparkContext.parallelize(predArray).map(x => Row(x))
    val predDF = sqlContext.createDataFrame(predictions, StructType(Array(StructField("prediction", DoubleType, nullable=false))))        
    return predDF
  }
  
  override def getConfigString(): String = {
    var strOut = ""
    config.foreach { case(key, value) => strOut += key + "=" + value.toString() + ";" }
    strOut += "["+this.baseLearner.toString()+":"+this.baseLearner.getConfigString()+"]"
    return strOut
  }
}