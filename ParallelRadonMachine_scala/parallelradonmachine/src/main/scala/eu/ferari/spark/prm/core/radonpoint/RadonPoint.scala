package eu.ferari.spark.prm.core.radonpoint

import org.apache.spark.sql.SQLContext
import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.mllib.linalg.{Matrix, SingularValueDecomposition, Vector, Vectors}
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.sql.DataFrame
import scala.runtime.ScalaRunTime.stringOf
import eu.ferari.spark.prm.core.Utils.transposeRowMatrix
import eu.ferari.spark.prm.core.learner.Learner
import scala.collection.parallel.mutable.ParArray

object RadonPoint {
  def getRadonPoint (S:Array[Array[Double]], sqlContext: SQLContext) : Array[Double] ={
    val n = S.length
    val dim = S(0).length
    
    val A_sparse  = S.map(a=>a :+1.0)
    val A         = sqlContext.sparkContext.parallelize(A_sparse.toArray.map(Vectors.dense))
    val Arow      = new RowMatrix(A)
    val rmA       = transposeRowMatrix(Arow)
    val svd: SingularValueDecomposition[RowMatrix, Matrix] = rmA.computeSVD(n, computeU = true)
    val svdVector = svd.V.toArray
    val alpha = svdVector.slice(svdVector.size-n,svdVector.size)

    var index =0
    var positiveAlphas  = new Array[Double](0)
    var positiveIndex   = new Array[Int](0)
    var negativeAlphas  = new Array[Double](0)
    var negativeIndex   = new Array[Int](0)

    while (index < alpha.length){
      if (alpha(index)>=0){
        positiveIndex = positiveIndex :+ index
        positiveAlphas = positiveAlphas :+ alpha(index)
      }
      else {
        negativeIndex = negativeIndex :+ index
        negativeAlphas = negativeAlphas :+ alpha(index)
      }
      index +=1
    }

    var sumPositiveAlphas = 0.0
    var testSum = 0.0
    index=0
    while (index < positiveAlphas.length) {
      sumPositiveAlphas += positiveAlphas(index)
      index += 1
    }
    index=0
    while (index < negativeAlphas.length) {
      testSum += negativeAlphas(index)
      index += 1
    }
    val epsilon = 0.00001
    if (sumPositiveAlphas + testSum > epsilon) {
      println("Error: sum of positive and negative alphas is not equal: %.5f <>".format(sumPositiveAlphas) + " %.5f".format(-1.0*testSum))
    }

    var radonPoint = new Array[Double](dim)
    for(idx <- positiveIndex){
      radonPoint = radonPoint.zip(S(idx).map(_ * alpha(idx))).map { case (x, y) => x + y }
    }
    radonPoint = radonPoint.map(_/sumPositiveAlphas)    
    return radonPoint
  }  
  
  def getIteratedRadonPoint (S:Array[Array[Double]], h:Int, radonNumber:Int, sqlContext: SQLContext) : Array[Double] ={
    if (h == 1){
      return getRadonPoint(S, sqlContext)
    }
    var Ssets = S.sliding(S.size/radonNumber,S.size/radonNumber).toArray
    var S_rec = Ssets.map(getRadonPoint(_, sqlContext))
    for (i <- 2 to (h-1)) {
      S_rec = Ssets.map(getRadonPoint(_, sqlContext))
    }
    return getRadonPoint(S_rec, sqlContext)
  }
  
  /**
   * The Radon number depends on the number of model parameters, thus it depends on the learner.
   */
  def getRadonNumber(data: DataFrame, learner: Learner[_], fraction: Double = 0.0) : Int = {
    var frac = fraction
    if (fraction == 0.0) {
      val N = data.count()
      frac = 100.0 / N.toFloat //use at least 100 samples
    }
    var sample = data.sample(false, frac)
    var locmodel = learner.fit(sample)
    var weights = locmodel.getParameters()
    return weights.length + 2
  }
}