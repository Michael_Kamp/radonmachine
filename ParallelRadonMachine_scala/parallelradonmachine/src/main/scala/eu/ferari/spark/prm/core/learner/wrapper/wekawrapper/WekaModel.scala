package eu.ferari.spark.prm.core.learner.wrapper.wekawrapper

import java.util.ArrayList;
import eu.ferari.spark.prm.core.learner.{Model}
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.sql.Row
import weka.core.{Instances, Instance, DenseInstance, Attribute}
import weka.classifiers.functions.{Logistic, LinearRegression, SGD, MultilayerPerceptron}
import weka.classifiers.functions.neural.{NeuralConnection, NeuralNode}
import scala.collection.mutable.ArrayBuffer

abstract class WekaModel[CLASSIFIER] extends Model {
  var model:CLASSIFIER
  
  def setModel(model : CLASSIFIER) {
    this.model = model
  }    
  
  def dataFrameToInstances(data: Dataset[Row], isRegression: Boolean = false): Instances = {
    import data.sqlContext.implicits._
    var attributes = new ArrayList[Attribute]()
    var labelAtt = new Attribute("label")
    if (!isRegression) { //if the task is classification or multi-class, the label attribute has to be nominal
      val labels = (data.select("label").distinct.map{r => r(0).asInstanceOf[Double]}.collect().toArray.map(x => x.toString))
      val labelsArrayList: ArrayList[String] = new ArrayList()
      labels.map(x => labelsArrayList.add(x))    
      labelAtt = new Attribute("label", labelsArrayList)
    }
    attributes.add(labelAtt)
    var exampleInstance = data.head()
    var features = exampleInstance.getAs[org.apache.spark.ml.linalg.Vector]("features").toArray
    for (i <- List.range(0, features.length)) {
      attributes.add(new Attribute("x"+i.toString()))
    }    
    var instances = new Instances("data", attributes, data.count().toInt)
    instances.setClass(labelAtt)
    val numAtts = instances.numAttributes()
    var listOfRows = data.collect().toList
    listOfRows.map(row => addRow(row, instances, numAtts))
    return instances
  }
  
  def addRow(row: Row, instances: Instances, numAtts: Int): Unit = {
    var label = row.getAs[Double]("label");   
    var feats = row.getAs[org.apache.spark.ml.linalg.Vector]("features").toArray;                     
    var vals = new Array[Double](numAtts)
    vals(0) = label
    for (i <- List.range(0, feats.length)) {
      vals(i+1) = feats(i)
    }
    var inst = new DenseInstance(1.0, vals)
    instances.add(inst)
  }
  
  def predict(data: Dataset[Row]) : Array[Double] = {
    return _predict(dataFrameToInstances(data))
  }
  
  def _predict(instances: Instances) : Array[Double]
  
  def flatDouble(in: Array[_ <: Any]): Array[Double] = in.flatMap{
        case i: Double => Array(i)
        case ai: Array[Double] => ai
        case x: Array[_] => flatDouble(x.toArray[Any])
  }
  
  /**
   * In WEKA, the coefficient structure is the following: 
   * the first dimension indexes the attributes, and the second the classes.
   */
  def structDouble(in: Array[Double], coefs: Array[Array[Double]]): Array[Array[Double]] = {
    var returnCoefficients = new Array[Array[Double]](coefs.size)
    var iPos = 0
    for (i <- List.range(0, coefs.size)) {
      returnCoefficients(i) = new Array[Double](coefs(i).size)
      for (j <- List.range(0, coefs(i).size)) {
        returnCoefficients(i)(j) = in(iPos)
        iPos += 1
      }
    }
    return returnCoefficients
  }
}

class WekaLogisticModel extends WekaModel[Logistic] {
  var model = new Logistic()
  
  def setParameters(parameters: Array[Double]): Unit = {
    var coefficients = structDouble(parameters, model.coefficients())
    var clazz = model.getClass()
    var field = clazz.getDeclaredField("m_Par");
    field.setAccessible(true)
    field.set(model, coefficients)
  }
  
  def getParameters(): Array[Double] = {
//    var coefficients = model.coefficients();						
//	  var weights = new Array[Double](coefficients.length);							
//		for (i <- List.range(0, coefficients.length)) {
//			for (j <- List.range(0, coefficients.length)) {
//				weights(i) = coefficients(i)(j);
//			}
//		}
//    return weights
    return flatDouble(model.coefficients())
  }
  
  def _predict(instances: Instances) : Array[Double] = {
    var predictions = new Array[Double](instances.size())
    for (i <- List.range(0, instances.size())) {
      predictions(i) = model.classifyInstance(instances.get(i))
    }
    return predictions
  }
}

class WekaLinearRegressionModel extends WekaModel[LinearRegression] {
  var model = new LinearRegression()
  
  def setParameters(parameters: Array[Double]): Unit = {
    var clazz = model.getClass()
    var field = clazz.getDeclaredField("m_Coefficients");
    field.setAccessible(true)
    field.set(model, parameters)
  }
  
  def getParameters(): Array[Double] = {
    return model.coefficients()
  }
  
  def _predict(instances: Instances) : Array[Double] = {
    var predictions = new Array[Double](instances.size())
    for (i <- List.range(0, instances.size())) {
      predictions(i) = model.classifyInstance(instances.get(i))
    }
    return predictions
  }
}

class WekaSGDModel extends WekaModel[SGD] {
  var model = new SGD()
  
  def setParameters(parameters: Array[Double]): Unit = {
    var clazz = model.getClass()
    var field = clazz.getDeclaredField("m_weights");
    field.setAccessible(true)
    field.set(model, parameters)
  }
  
  def getParameters(): Array[Double] = {
    return model.getWeights()
  }
  
  def _predict(instances: Instances) : Array[Double] = {
    var predictions = new Array[Double](instances.size())
    for (i <- List.range(0, instances.size())) {
      predictions(i) = model.classifyInstance(instances.get(i))
    }
    return predictions
  }
}

class WekaMLPModel extends WekaModel[MultilayerPerceptron] {
  var model = new MultilayerPerceptron()
  
  def setParameters(parameters: Array[Double]): Unit = {
    var clazz = model.getClass()
    var field = clazz.getDeclaredField("m_neuralNodes");
    field.setAccessible(true)
    var m_neuralNodes = field.get(model).asInstanceOf[Array[NeuralConnection]]
    var params = parameters.toBuffer
    for (m_neuralNode <- m_neuralNodes) {
       var con = m_neuralNode.asInstanceOf[NeuralNode]; 
       var conclazz = con.getClass()
       var confield = conclazz.getDeclaredField("m_weights");
       confield.setAccessible(true)
       var m_weights = confield.get(con).asInstanceOf[Array[Double]]       
       for (i <- List.range(0, m_weights.length)) {
         m_weights(i) = params(i)         
       }
       params.remove(0, m_weights.length)
       confield.set(con, m_weights)
    }
    //println(model.toString())
  }
  
  def getParameters(): Array[Double] = {
    var clazz = model.getClass()
    var field = clazz.getDeclaredField("m_neuralNodes");
    field.setAccessible(true)
    var m_neuralNodes = field.get(model).asInstanceOf[Array[NeuralConnection]]
    var parameters = Array.emptyDoubleArray
    for (m_neuralNode <- m_neuralNodes) {
      var con = m_neuralNode.asInstanceOf[NeuralNode]; 
      var weights = con.getWeights();
      var inputs = con.getInputs();
      parameters = parameters ++ weights
    }
    //println(model.toString())
    return parameters
  }
  
  def _predict(instances: Instances) : Array[Double] = {
    var predictions = new Array[Double](instances.size())
    for (i <- List.range(0, instances.size())) {
      predictions(i) = model.classifyInstance(instances.get(i))
    }
    return predictions
  }
}