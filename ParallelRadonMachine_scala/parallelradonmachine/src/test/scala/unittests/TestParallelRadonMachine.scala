package unittests

//import unittests.TestCase
import eu.ferari.spark.prm.core.ParallelRadonMachine
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.{SQLContext, DataFrame}
import eu.ferari.spark.prm.core.radonpoint.RadonPoint
import eu.ferari.spark.prm.core.learner.{Learner, Model}
import scala.math.abs
import scala.runtime.ScalaRunTime.stringOf
import scala.reflect._
import eu.ferari.spark.prm.core.learner.wrapper.sparkwrapper._
import org.apache.spark.mllib.classification.{SVMWithSGD, SVMModel, LogisticRegressionWithSGD, LogisticRegressionModel, LogisticRegressionWithLBFGS}
import org.apache.spark.ml.classification.{LogisticRegression, MultilayerPerceptronClassifier}
import weka.classifiers.functions.{Logistic, LinearRegression}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import eu.ferari.spark.prm.experiments.datautils.DataUtil
import org.junit.Test

object TestParallelRadonMachine extends TestParallelRadonMachine{
  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Parallel Radon Machine").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  }
}

class TestParallelRadonMachine extends TestCase {    
  
  @Test def run{
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Parallel Radon Machine").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
    
    sc.stop()
  }
  
  def run(sqlContext: SQLContext): Unit = { 
    /* Read dummy test data */    
    val data = DataUtil.getDataset("dummyDataBinClass_libsvm", sqlContext)//sqlContext.read.format("libsvm").load(getClass.getResource("").getPath()+"../../../../src/main/resources/prm/data/classification/dummyDataBinClass_libsvm.txt")
    //val data = sqlContext.read.format("libsvm").load("C:/Research/Development/scala/parallelradonmachine/data/classification/dummyDataBinClass_libsvm.txt")
    
    /* Test PRM on MLP and dummy test data (model will be bad because of too few training data instances, but no error should occur) */
    val splits = data.randomSplit(Array(0.7,0.3))
    val traindata = splits(0)
    val testdata = splits(1)
    
    var opt = Learner(classTag[MultilayerPerceptronClassifier])
    assert(opt.isDefined, "Error: Factory returned None for LogisticRegressionWithLBFGS.")    
    var learner = opt.get
    learner.setConfig(scala.collection.mutable.Map[String, Any]("layer" -> Array[Int](3,2,2)))
    
    val prm = new ParallelRadonMachine(learner)
    prm.setConfig(scala.collection.mutable.Map[String, Any]("h"-> -1))
    val model = prm.fit(traindata, stratifiedSample = true)
    val preds = model.predict(testdata)
    //println(stringOf(preds.select("prediction").collect()))
    //println(stringOf(testdata.select("label").collect()))    
    
    /* Testing PRM on all wrapped learners */    
    var learnerBaseClasses = Learner.getWrappedLearners()    
    learnerBaseClasses.map(l => testParallelRadonMachine(sqlContext, l, data, false))    
  } 
  
  def testParallelRadonMachine(sqlContext: SQLContext, baseLearnerClass: ClassTag[_], data: DataFrame, throwAssertions: Boolean = true) {
    val splits = data.randomSplit(Array(0.7,0.3))
    val traindata = splits(0)
    val testdata = splits(1)
    
    var coefs = Array(-10.0,-10.0,-10.0,-10.0)
    
    print("Testing PRM with baseClass="+stringifyClass(baseLearnerClass)+" init_wrapper=")
    try {
      var opt = Learner(baseLearnerClass)
      if (throwAssertions) {
        assert(opt.isDefined, "Error: Factory returned None for learner "+stringOf(baseLearnerClass)+".")
      }
      var newlearner = opt.get
      if (stringifyClass(baseLearnerClass).contains("MultilayerPerceptronClassifier")) {
        newlearner.setConfig(scala.collection.mutable.Map[String, Any]("layer" -> Array[Int](3,2,2)))
      }
      print("ok.     init_prm=")
      val prm = new ParallelRadonMachine(newlearner)
      print("ok.     fit=")
      var newmodel = prm.fit(traindata)
      print("ok.     predict=")
      var preds = newmodel.predict(testdata)
      print("ok.     get_model_parameters=")
      coefs = newmodel.getParameters()
      print("ok.     ")
    } catch {
      case e: Exception => print("failed. ")
    }
    
    var weightsSomewhatCorrect = (coefs(0) > 0) & (coefs(1) > 0) & (coefs(2) < 0) & (coefs(3) >= 0) & (coefs(1) < abs(coefs(2)))  | coefs.length > 4
    if (throwAssertions) {
      assert(weightsSomewhatCorrect, "Model coefficients are far off from correct. Expected coefficients are (0.3, 0.5, -0.7, 0.5) but learner gave "+coefs.mkString("(", ",", ")")+".")
    }
    
    if (weightsSomewhatCorrect) {
      println(" test = successful.")
    }
    else {
      println(" test = failed.")
    }
  }
}