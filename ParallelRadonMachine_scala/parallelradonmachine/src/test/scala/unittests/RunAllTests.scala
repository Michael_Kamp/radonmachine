package unittests

import unittests._
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.junit.Test

object RunAllTests extends TestCase{
  var testCases: List[TestCase] = List(new TestBaselines, new TestExperiment, new TestLearnerWrapper, new TestParallelRadonMachine, new TestRadonPointCalculation, new TestMetrics)
  
  def main(args: Array[String]): Unit = {
    println("Starting Program!")
    //System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").set("spark.driver.memory", "16g").set("spark.driver.maxResultSize","4g").set("spark.akka.frameSize", "2047").set("spark.executor.memory", "8g").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    if (args.length > 0) {
      run(args, sqlContext)
    }
    else {
      run(sqlContext)
    }
  }
  
  def runAsUnitTest() {
    val conf = new SparkConf().setAppName("Unit-Test").set("spark.driver.memory", "16g").set("spark.driver.maxResultSize","4g").set("spark.akka.frameSize", "2047").set("spark.executor.memory", "8g").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  }
  
  def run(testcases: Array[String], sqlContext: SQLContext): Unit = {
    for (testCaseName <- testcases) {
        var res = testCases.filter(x => x.getClass.toString().contains(testCaseName))
        if (res.length > 0) {
          res(0).run(sqlContext)
        }
        else {
          println("Testcase \""+testCaseName+"\" not found.")
        }
      }
  }
  
  def run(sqlContext: SQLContext): Unit = {  
    for (test <- testCases) {
        test.run(sqlContext)
      }
  }
}