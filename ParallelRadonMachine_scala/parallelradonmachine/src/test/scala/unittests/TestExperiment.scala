package unittests

//import unittests.TestCase
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import eu.ferari.spark.prm.core.learner.{Learner, Model, AverageZeroOneLoss, AverageHingeLoss, ACC, AUC}
import eu.ferari.spark.prm.core.ParallelRadonMachine
import eu.ferari.spark.prm.experiments.Experiment
import org.junit.Test

object TestExperiment extends TestExperiment {
  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Experiments").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  }
}

class TestExperiment extends TestCase{
  
  @Test def run {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Experiments").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
    
    sc.stop()
  }
  
  def run(sqlContext: SQLContext): Unit = {   
    var datasets = Array("dummyDataBinClass_libsvm")
    var learners: Array[Learner[Model]] = Learner.getWrappedLearners().map(l => Learner(l).get.asInstanceOf[Learner[Model]])
    var radonMachines = learners.map(l => new ParallelRadonMachine(l).asInstanceOf[Learner[Model]]).toArray
    learners = learners ++ radonMachines
    var metrics = Array(AverageZeroOneLoss, AverageHingeLoss, ACC, AUC)
    
    
    val nFolds = 2
    val exp = new Experiment("Testcase_TestExperiment")
    exp.run(sqlContext, datasets, learners, metrics, nFolds)        
  }
}