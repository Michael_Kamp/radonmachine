<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>eu.ferari.spark.prm</groupId>
  <artifactId>parallelradonmachine</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <name>${project.artifactId}</name>
  <description>The parallel Radon machine is a general parallelisation scheme for machine learning algorithms.</description>
  <inceptionYear>2016</inceptionYear>
  <licenses>
    <license>
      <name>Apache License 2.0</name>
      <url>https://www.apache.org/licenses/LICENSE-2.0.</url>
      <distribution>https://bitbucket.org/Michael_Kamp/parallelradonmachine</distribution>
    </license>
  </licenses>

  <properties>
    <maven.compiler.source>1.6</maven.compiler.source>
    <maven.compiler.target>1.6</maven.compiler.target>
    <encoding>UTF-8</encoding>
    <scala.version>2.11.8</scala.version>
    <scala.compat.version>2.11</scala.compat.version>
  </properties>

  <dependencies>
    <dependency>
      <groupId>org.scala-lang</groupId>
      <artifactId>scala-library</artifactId>
      <version>${scala.version}</version>
    </dependency>
	<dependency>
	   <groupId>org.scala-lang</groupId>
	   <artifactId>scala-reflect</artifactId>
	   <version>${scala.version}</version>
	</dependency>
    <!-- Test -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.specs2</groupId>
      <artifactId>specs2-core_${scala.compat.version}</artifactId>
      <version>3.7</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.scalatest</groupId>
      <artifactId>scalatest_${scala.compat.version}</artifactId>
      <version>3.0.1</version>
      <scope>test</scope>
    </dependency>
    <dependency>
		<groupId>org.apache.spark</groupId>
		<artifactId>spark-core_2.11</artifactId>
		<version>2.0.2</version>
	</dependency>
	<dependency>
		<groupId>org.apache.spark</groupId>
		<artifactId>spark-sql_2.11</artifactId>
		<version>2.0.2</version>
	</dependency>
	<dependency>
		<groupId>org.apache.spark</groupId>
		<artifactId>spark-mllib_2.11</artifactId>
		<version>2.0.2</version>
	</dependency>
    <dependency>
		<groupId>nz.ac.waikato.cms.weka</groupId>
		<artifactId>weka-stable</artifactId>
		<version>3.8.0</version>
	</dependency>
	<dependency>
		<groupId>org.reflections</groupId>
		<artifactId>reflections</artifactId>
		<version>0.9.9-RC1</version>
	</dependency>
	<dependency>
		<groupId>org.apache.hadoop</groupId>
		<artifactId>hadoop-client</artifactId>
		<version>2.7.3</version>
	</dependency>
	<!-- <dependency>
    	<groupId>WrapsLearner</groupId>
    	<artifactId>prm</artifactId>
    	<version>1.0</version>
    	<scope>system</scope>
    	<systemPath>${project.basedir}/src/main/resources/annotations.jar</systemPath>
	</dependency>  -->
	<dependency>
    	<groupId>eu.ferari.spark.prm</groupId>
    	<artifactId>annotations</artifactId>
    	<version>1.0</version>
	</dependency>
  </dependencies>

  <build>
    <sourceDirectory>src/main/scala</sourceDirectory>
    <testSourceDirectory>src/test/scala</testSourceDirectory>
    <plugins>
      <plugin>
        <!-- see http://davidb.github.com/scala-maven-plugin -->
        <groupId>net.alchim31.maven</groupId>
        <artifactId>scala-maven-plugin</artifactId>
        <version>3.2.2</version>
        <executions>
          <execution>
            <goals>
              <goal>compile</goal>
              <goal>testCompile</goal>
            </goals>
            <configuration>
              <args>
                <arg>-dependencyfile</arg>
                <arg>${project.build.directory}/.scala_dependencies</arg>
              </args>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.18.1</version>
        <configuration>
          <useFile>false</useFile>
          <disableXmlReport>true</disableXmlReport>
          <!-- If you have classpath issue like NoDefClassError,... -->
          <!-- useManifestOnlyJar>false</useManifestOnlyJar -->
          <includes>
            <include>**/*Test.*</include>
            <include>%regex[.*unittests.*Test.*]</include>
          </includes>
        </configuration>
      </plugin>
	  <plugin>
		<groupId>org.apache.maven.plugins</groupId>
		<artifactId>maven-shade-plugin</artifactId>
		<version>2.4.3</version>
		<executions>
			<execution>
				<phase>package</phase>
				<goals>
					<goal>shade</goal>
				</goals>
				<configuration>
					<transformers>
						<transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
							<mainClass>eu.ferari.spark.prm.parallelradonmachine.experiments.MLJ.classification.RadonVsSparkLearners</mainClass>
						</transformer>
						<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
							<resource>reference.conf</resource>
						</transformer>
          					<transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer"/>															
					</transformers>
				    <filters>
						<filter>
				            <artifact>*:*</artifact>
				            <excludes>
				                <exclude>META-INF/*.SF</exclude>
				                <exclude>META-INF/*.DSA</exclude>
				                <exclude>META-INF/*.RSA</exclude>
				            </excludes>
				        </filter>
					</filters>							
				</configuration>
			</execution>
		</executions>
		<configuration>
			<finalName>parallelRadonMachine</finalName>
		</configuration>
	</plugin>
	<plugin>
         <groupId>org.eclipse.m2e</groupId>
         <artifactId>lifecycle-mapping</artifactId>
         <version>1.0.0</version>
         <configuration>
           <lifecycleMappingMetadata>
             <pluginExecutions>
               <pluginExecution>
                 <!-- Configure the install-file goal from the install plugin to be
                      executed during project configuration but not during incremental
                      builds. In particular, this will trigger the execution with id
                      'create-processingutils-artifact' configured above.
                      N.B. this execution will, by limitation of m2e, also be triggered
                      on full project builds. -->
                 <pluginExecutionFilter>
                   <groupId>org.apache.maven.plugins</groupId>
                   <artifactId>maven-install-plugin</artifactId>
                   <versionRange>[2.5.1,)</versionRange>
                   <goals>
                     <goal>install-file</goal>
                   </goals>
                 </pluginExecutionFilter>
                 <action>
                   <execute>
                     <runOnConfiguration>true</runOnConfiguration>
                     <runOnIncremental>false</runOnIncremental>
                   </execute>
                 </action>
               </pluginExecution>
             </pluginExecutions>
           </lifecycleMappingMetadata>
         </configuration>
       </plugin>
    </plugins>
  </build>
</project>
