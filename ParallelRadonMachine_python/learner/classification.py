from framework.logger import log
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.svm import LinearSVC as SKLearnLinearSVC
from datetime import datetime
from learner import Learner
import numpy as np
#from scipy.special.basic import lmbda


class SKLearnLinearClassifier(Learner):
    def __init__(self):
        Learner.__init__(self)
        self.identifier = "GenericSKLearnLinearClassifier (please replace)"
        self.learner = None        
        
    def train(self, X, y):
        start = datetime.now()
        self.learner.fit(X,y)
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()
    
    def predict(self, X):
        return self.learner.predict(X)
    
    def getModel(self):
        return self.learner.coef_
    
    def setModel(self, model):        
        if self.learner.coef_.shape != model.shape:
            log( "Error: new model shape does not fit current model shape. ", newline = False)
            log( "Expected: "+str(self.learner.coef_.shape)+" but got "+str(model.shape)+".")
        else:
            self.learner.coef_ = model

class RandomClassifier(SKLearnLinearClassifier): #for debug purposes only
    def __init__(self):
        SKLearnLinearClassifier.__init__(self)
        self.identifier = "Random"
        self.learner = SGDClassifier()
        
    def train(self, X, y):
        start = datetime.now()
        self.learner.fit(X,y)
        self.learner.coef_ = np.random.rand(self.learner.coef_.shape[0],self.learner.coef_.shape[1])
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()
            
class StochasticGradientDescent(SKLearnLinearClassifier):
    def __init__(self, lmbda = 0.0001, eta = 'optimal', loss="squared_hinge", penalty="l2", shuffle=True, n_iter = 5):
        SKLearnLinearClassifier.__init__(self)
        self.identifier = "SGD"
        self.learner = SGDClassifier(alpha = lmbda, learning_rate = eta, loss=loss, penalty=penalty, shuffle=shuffle, n_iter = n_iter)
        self.lmbda = lmbda
        self.eta = eta
        self.loss = loss
        self.penalty = penalty
        self.shuffle = shuffle
        self.n_iter = n_iter
    
    def getInitParams(self):
        return {'lmbda':self.lmbda, 'eta':self.eta, 'loss':self.loss, 'penalty':self.penalty, 'shuffle':self.shuffle, 'n_iter':self.n_iter}        
            
class LinearSVC(SKLearnLinearClassifier):
    def __init__(self, C = 1.0, loss = 'squared_hinge', penalty = 'l2', dual = False): #dual = false is useful for N >> d, which we usually assume for RadonPoint-Boosting                
        SKLearnLinearClassifier.__init__(self)
        self.identifier = "LinearSVC"
        self.learner = SKLearnLinearSVC(C=C, loss = loss, penalty = penalty, dual = dual)
        self.loss = loss
        self.penalty = penalty
        self.dual = dual
        self.C = C
    
    def getInitParams(self):
        return {'C':self.C, 'loss':self.loss, 'penalty':self.penalty, 'dual':self.dual}
    
class LogisticRegressionLBFGS(SKLearnLinearClassifier):
    def __init__(self, C = 1.0, penalty = 'l2', dual = False): #dual = false is useful for N >> d, which we usually assume for RadonPoint-Boosting
        SKLearnLinearClassifier.__init__(self)        
        self.identifier = "LogisticRegressionLBFGS"
        self.learner = LogisticRegression(C=C, penalty = penalty, dual = dual, solver='lbfgs')
        self.penalty = penalty
        self.dual = dual
        self.C = C
    
    def getInitParams(self):
        return {'C':self.C, 'penalty':self.penalty, 'dual':self.dual}
    
class LogisticRegressionNewtonCG(SKLearnLinearClassifier):
    def __init__(self, C = 1.0, penalty = 'l2', dual = False): #dual = false is useful for N >> d, which we usually assume for RadonPoint-Boosting
        SKLearnLinearClassifier.__init__(self)        
        self.identifier = "LogisticRegressionNewtonCG"
        self.learner = LogisticRegression(C=C, penalty = penalty, dual = dual, solver='newton-cg')
        self.penalty = penalty
        self.dual = dual
        self.C = C
    
    def getInitParams(self):
        return {'C':self.C, 'penalty':self.penalty, 'dual':self.dual}
    
class LogisticRegressionSAG(SKLearnLinearClassifier):
    def __init__(self, C = 1.0, penalty = 'l2', dual = False): #dual = false is useful for N >> d, which we usually assume for RadonPoint-Boosting
        SKLearnLinearClassifier.__init__(self)        
        self.identifier = "LogisticRegressionSAG"
        self.learner = LogisticRegression(C=C, penalty = penalty, dual = dual, solver='sag')
        self.penalty = penalty
        self.dual = dual
        self.C = C
    
    def getInitParams(self):
        return {'C':self.C, 'penalty':self.penalty, 'dual':self.dual}
    