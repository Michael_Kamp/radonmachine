olog4jLogger = None

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(mode = 'python')
@static_vars(SPARK = 'spark')
@static_vars(PYTHON = 'python')
@static_vars(sc = None)
def log(logmessage, *args, **kwargs):
    if log.mode == log.PYTHON:
        print logmessage, 
        for a in args:
            print a,
        if 'newline' in kwargs:
            if kwargs['newline'] == True:        
                print ""            
    elif log.mode == log.SPARK:
        import pyspark
        import logging
        print "*"
        print "*"
        print "*"
        print logmessage,
        for a in args:
            print a,
        if 'newline' in kwargs:
            if kwargs['newline'] == True:        
                print ""
        print "*"
        print "*"
        print "*"
        #global olog4jLogger
        #if olog4jLogger == None:
        #    olog4jLogger = log.sc._jvm.org.apache.log4j
        #    olog4jLogger.LogManager.getLogger("org").setLevel( olog4jLogger.Level.OFF )
        #    olog4jLogger.LogManager.getLogger("akka").setLevel( olog4jLogger.Level.OFF )
#         if not isinstance(logmessage, basestring):
#             logmessage = str(logmessage)
#         logstring = logmessage 
#         for a in args:
#             logstring += " " + str(a)        
#          
#         LOGGER = olog4jLogger.LogManager.getLogger('ExpFramework')
#         LOGGER.info(logstring)
#         LOGGER.debug(logstring)
    else:
        print logmessage, args 