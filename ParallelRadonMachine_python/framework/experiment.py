from datetime import datetime
import gc
import sys, os, time
import random
import math
import pickle
import numpy as np
from radonPoints import radonBoosting
from radonPoints.radonBoosting import RadonBoost
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold, KFold
from data.datasets import Datasets
from framework import evaluation
from framework.output import Output, CENTRAL, TIMED
from framework.sampleSizes import sampleSizeLinear
from pandas.lib import indices_fast
import radonPoints.radonComputation as rc
from framework.logger import log


#from output import Output
time_format = '%d.%m.%Y %H:%M:%S'
time_folder_format = '%Y-%m-%d %H-%M-%S'

NOPARALLELSPARK = True

class Experiment:
    def __init__(self, datasets, methods, metrics):
        self.dataHandler = Datasets()
        self.datasets = datasets
        self.methods = methods        
        self.metrics = metrics
        self.scores = {}
        self.durations = {}
        self.datasetInfo = {}        
        for method in self.methods:
            self.scores[str(method)] = {}
            self.durations[str(method)] = {}            
            for dataset in datasets:
                self.scores[str(method)][dataset] = {}
                self.durations[str(method)][dataset] = 0.0                
                for metric in self.metrics:
                    self.scores[str(method)][dataset][metric] = 0.0
        self.current_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
    
    def expTimerStart(self, bSilent=True):
        self.startTime = time.time()
        timestamp = time.strftime(time_format, time.localtime(self.startTime))
        if not bSilent:
            log( "Experiment started at %s" % (timestamp))     
        return self.startTime
    
    def expTimerStop(self, bSilent=True):
        self.endTime = time.time()
        if not bSilent:
            log( "Experiment ended at %s" % (time.strftime(time_format, time.localtime(self.endTime))))
        seconds = self.endTime - self.startTime
        minutes = int(seconds) / 60        
        rest = int(seconds - minutes * 60)            
        if not bSilent:
            log( "Duration: %d minutes and %d seconds" % ((int(minutes)), int(rest)))

class ParallelRadonExperiment(Experiment):
    def __init__(self, datasets, dataHandler, learner, metrics, paralllelizationClasses = None, regression = False):        
        self.dataHandler = dataHandler
        self.datasets = datasets
        self.learner = learner
        self.bRegression = regression
        self.radonLearner = []
        self.methods = []
        self.paralllelizationClasses = paralllelizationClasses if paralllelizationClasses != None else [RadonBoost]
        for l in learner:
            self.methods.append(l)
            for paraClass in self.paralllelizationClasses:
                if paraClass.__name__ == "SparkRadonBoostOnLearnerSet":
                    continue
                r = paraClass(method = l.__class__(**l.getInitParams()))
                self.radonLearner.append(r)
                self.methods.append(r)                            
        self.metrics = metrics
        self.scores = {}
        self.durations = {}
        self.datasetInfo = {}
        self.actualNForTimedCentralExps = {}
        for method in self.methods:            
            self.scores[str(method)] = {}
            self.durations[str(method)] = {}
            self.actualNForTimedCentralExps[str(method)] = {}
            for dataset in datasets:
                self.scores[str(method)][dataset] = {}
                self.durations[str(method)][dataset] = {}
                self.actualNForTimedCentralExps[str(method)][dataset] = {}
                for metric in self.metrics:
                    self.scores[str(method)][dataset][metric] = {}
        self.current_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        
    def run(self, folds = 10, bRunTimedExperiment = True):
        #create experiment subfolder with timestamp
        timeString = time.strftime(time_folder_format, time.localtime(time.time()))
        folderPath = os.path.join(self.current_dir, timeString)
        os.mkdir(folderPath)
        self.exp_dir = folderPath
        #start experiment
        self.expTimerStart(False)
        for dataset in self.datasets:
            self.datasetInfo[dataset] = self.dataHandler.getDatasetInfo(dataset)            
            log( "Dataset "+dataset+" "+str(self.datasetInfo[dataset]['task'])+" ("+str(self.datasetInfo[dataset]['N'])+"x"+str(self.datasetInfo[dataset]['D'])+") :")
            X,y = self.dataHandler.getDataset(dataset)     
            if not self.bRegression:   
                kf = StratifiedKFold(y, folds, shuffle=True) #generate one unused sample, because the last sample has a different number of elements and we throw it away 
            else:
                kf = KFold(len(X), n_folds=folds)
            foldIdxs = [(trainIDX, testIDX) for trainIDX, testIDX in kf]
            method = self.learner[0]
            method.train(X,y)
            w = method.getModel()
            r = w.shape[1] + 2  #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)
            N = int(len(X) / folds)*(folds-1)
            for iFold in xrange(folds):
                log( "Fold ",str(iFold),": ", newline = False)
                trainIDX, testIDX = foldIdxs[iFold]
                X_train = X[trainIDX]
                X_test  = X[testIDX]
                y_train = y[trainIDX]
                y_test  = y[testIDX]                                
                for learner in self.learner:
                    self.runCentralExperiment(learner, dataset, X_train, y_train, X_test, y_test, iFold)                                                                                  
                    h = 1
                    n = int(float(N) / (float(r)**h))
                    while n >= 2:                                    
                        I = range(len(X_train))
                        random.shuffle(I)
                        idx = np.array(I)
                        ShuffledX = np.array(X_train)[idx]
                        ShuffledY = np.array(y_train)[idx]
                        h = int(math.log(float(N)/float(n))/math.log(float(r))) #floor of that number 
                        for paraClass in self.paralllelizationClasses:                            
                            method = paraClass(method = learner.__class__(**learner.getInitParams()))
                            trainduration = self.runParallelExperiment(method, dataset, ShuffledX, ShuffledY, X_test, y_test, iFold, n, h)
                            if 'RadonBoost' in str(method) and bRunTimedExperiment:
                                self.runCentralExperimentSameTime(learner, dataset, X_train, y_train, X_test, y_test, iFold, trainduration, n, h)      
                        gc.collect()     
                        h += 1
                        n = int(float(N) / (float(r)**h))    
#                     for n in sampleSetSizes:                                        
#                         I = range(len(X_train))
#                         random.shuffle(I)
#                         idx = np.array(I)
#                         ShuffledX = np.array(X_train)[idx]
#                         ShuffledY = np.array(y_train)[idx]
#                         h = int(math.log(float(N)/float(n))/math.log(float(r))) #floor of that number 
#                         if h >= minH:
#                             self.runParallelExperiment(learner, dataset, ShuffledX, ShuffledY, X_test, y_test, iFold, n, h)        
#                         gc.collect()     
                    gc.collect()     
                gc.collect()
                log( "done.")
            gc.collect()     
                            
        self.expTimerStop(False)                                                                                  
        out = Output(self.exp_dir)
        out.writeParallelExperimentSummary(self.scores, self.durations, self.datasetInfo, self.methods, self.learner, self.radonLearner, self.metrics, self.startTime, self.endTime, folds, self.actualNForTimedCentralExps)
        out.createResultPlotterFile()
        log( self.scores)
        log( self.durations)
        log( self.datasetInfo)
                
    def runCentralExperiment(self, method, dataset, ShuffledX, ShuffledY, X_test, y_test, iFold):
        if not CENTRAL in self.durations[str(method)][dataset]: 
            self.durations[str(method)][dataset][CENTRAL] = {}
        if not iFold in self.durations[str(method)][dataset][CENTRAL]: 
            self.durations[str(method)][dataset][CENTRAL][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
        log( str(method) + " ... ", newline = False)     
        try:            
            trainduration, = method.train(ShuffledX, ShuffledY)
            start = datetime.now()
            y_pred = method.predict(X_test)
            stop = datetime.now()
            testduration = (stop - start).total_seconds()
            self.durations[str(method)][dataset][CENTRAL][iFold]['train'] = trainduration['train']
            self.durations[str(method)][dataset][CENTRAL][iFold]['sync'] = trainduration['sync']
            self.durations[str(method)][dataset][CENTRAL][iFold]['test'] = testduration
            self.durations[str(method)][dataset][CENTRAL][iFold]['total'] = trainduration['train']+trainduration['sync']+testduration                                
            for metric in self.metrics:
                if not CENTRAL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][CENTRAL] = {}
                self.scores[str(method)][dataset][metric][CENTRAL][iFold] = metric(y_pred, y_test)      
            gc.collect()     
        except:
            log( "error!", sys.exc_info()[0])
            self.durations[str(method)][dataset][CENTRAL][iFold]['train'] = -1.0
            self.durations[str(method)][dataset][CENTRAL][iFold]['sync'] = -1.0
            self.durations[str(method)][dataset][CENTRAL][iFold]['test'] = -1.0
            self.durations[str(method)][dataset][CENTRAL][iFold]['total'] = -1.0                                
            for metric in self.metrics:
                if not CENTRAL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][CENTRAL] = {}
                self.scores[str(method)][dataset][metric][CENTRAL][iFold] = 'error'
            gc.collect()         
            
    def runParallelExperiment(self, method, dataset, ShuffledX, ShuffledY, X_test, y_test, iFold, n, h):
        PARALLEL = (n,h)
        if "MinSample" in str(method):
            method.method.train(ShuffledX,ShuffledY)
            w = method.method.getModel()
            R = rc.getRadonNumber(w)
            min_n = len(method.getMinimalSample(range(len(ShuffledX)), ShuffledY, int(R**h))[0])                    
        if not PARALLEL in self.durations[str(method)][dataset]: 
            self.durations[str(method)][dataset][PARALLEL] = {}
        if not iFold in self.durations[str(method)][dataset][PARALLEL]: 
            self.durations[str(method)][dataset][PARALLEL][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
        log( str(method) + " ... ", newline = False)   
        try:            
            trainduration, = method.train(ShuffledX, ShuffledY, bRegression = self.bRegression)
            start = datetime.now()
            y_pred = method.predict(X_test)
            stop = datetime.now()
            testduration = (stop - start).total_seconds()
            self.durations[str(method)][dataset][PARALLEL][iFold]['train'] = trainduration['train']
            self.durations[str(method)][dataset][PARALLEL][iFold]['sync'] = trainduration['sync']
            self.durations[str(method)][dataset][PARALLEL][iFold]['test'] = testduration
            self.durations[str(method)][dataset][PARALLEL][iFold]['total'] = trainduration['train']+trainduration['sync']+testduration                                
            for metric in self.metrics:
                if not PARALLEL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][PARALLEL] = {}
                self.scores[str(method)][dataset][metric][PARALLEL][iFold] = metric(y_pred, y_test)
            if "MinSample" in str(method): 
                MIN_SAMPLE = "MinSample"+str(PARALLEL)  
                if MIN_SAMPLE not in self.actualNForTimedCentralExps[str(method)][dataset]:
                    self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE] = {}
                self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE][iFold] = min_n
            gc.collect()     
        except:
            log( "error: ", sys.exc_info()[0])
            self.durations[str(method)][dataset][PARALLEL][iFold]['train'] = -1.0
            self.durations[str(method)][dataset][PARALLEL][iFold]['sync'] = -1.0
            self.durations[str(method)][dataset][PARALLEL][iFold]['test'] = -1.0
            self.durations[str(method)][dataset][PARALLEL][iFold]['total'] = -1.0                                
            for metric in self.metrics:
                if not PARALLEL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][PARALLEL] = {}
                self.scores[str(method)][dataset][metric][PARALLEL][iFold] = float('NaN')
            if "MinSample" in str(method): 
                MIN_SAMPLE = "MinSample"+str(PARALLEL)  
                if MIN_SAMPLE not in self.actualNForTimedCentralExps[str(method)][dataset]:
                    self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE] = {}
                self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE][iFold] = min_n
            gc.collect()   
        return self.durations[str(method)][dataset][PARALLEL][iFold]['train']+self.durations[str(method)][dataset][PARALLEL][iFold]['sync']
        
    def runCentralExperimentSameTime(self, method, dataset, X_train, y_train, X_test, y_test, iFold, traindurationParallel, nParallel, h):        
        log( str(method),"Timed... ", newline = False)
        try:       
            PARALLEL = (nParallel,h)
            TIMED_PARAM = TIMED + str(PARALLEL)   
            nOld = -1
            trainduration = None
            N = len(X_train)
            n_min = len(np.unique(y_train))               
            min = n_min
            max = N  
            n = nParallel
            indices = np.arange(N)
            np.random.shuffle(indices)
            #sample = self.getSingleStratifiedSample(indices, y_train, n)
            sample = indices[:n]
            while len(np.unique(y_train[sample])) != len(np.unique(y_train)): #make this more efficient
                    log( "Error: stratified sampling went wrong...retry")
                    np.random.shuffle(indices)
                    sample = self.getSingleStratifiedSample(indices, y_train, n)
            if len(sample) != n:
                log( "Error: n should be ",n," but it's ",str(len(sample)),". Dajum!\n")
            X = X_train[sample]
            y = y_train[sample]   
            trainduration, = method.train(X, y)
#             n = (max + min) / 2           
#             while abs(nOld - n) > 2:               
#                 indices = np.arange(N)
#                 np.random.shuffle(indices)
#                 sample = self.getSingleStratifiedSample(indices, y_train, n)         
#                 while len(np.unique(y_train[sample])) != len(np.unique(y_train)): #make this more efficient
#                     log( "Error: stratified sampling went wrong...retry")
#                     np.random.shuffle(indices)
#                     sample = self.getSingleStratifiedSample(indices, y_train, n)
#                 X = X_train[sample]
#                 y = y_train[sample]   
#                 trainduration, = method.train(X, y)
#                 nOld = n
#                 if trainduration['train'] < traindurationParallel:                                        
#                     min = n + 1
#                     n = (max + min) / 2    
#                 elif trainduration['train'] > traindurationParallel:
#                     max = n + 1
#                     n = (max + min) / 2    
            start = datetime.now()
            y_pred = method.predict(X_test)
            stop = datetime.now()
            testduration = (stop - start).total_seconds()            
            if not TIMED_PARAM in self.durations[str(method)][dataset]: 
                self.durations[str(method)][dataset][TIMED_PARAM] = {}            
            if not iFold in self.durations[str(method)][dataset][TIMED_PARAM]: 
                self.durations[str(method)][dataset][TIMED_PARAM][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['train'] = trainduration['train']
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['sync'] = trainduration['sync']
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['test'] = testduration
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['total'] = trainduration['train']+trainduration['sync']+testduration                                
            for metric in self.metrics:
                if not TIMED_PARAM in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][TIMED_PARAM] = {}
                self.scores[str(method)][dataset][metric][TIMED_PARAM][iFold] = metric(y_pred, y_test)
            if TIMED_PARAM not in self.actualNForTimedCentralExps[str(method)][dataset]:
                self.actualNForTimedCentralExps[str(method)][dataset][TIMED_PARAM] = {}
            self.actualNForTimedCentralExps[str(method)][dataset][TIMED_PARAM][iFold] = n
            gc.collect()     
        except:
            log( "error!", sys.exc_info()[0])
            if not TIMED_PARAM in self.durations[str(method)][dataset]: 
                self.durations[str(method)][dataset][TIMED_PARAM] = {}            
            if not iFold in self.durations[str(method)][dataset][TIMED_PARAM]: 
                self.durations[str(method)][dataset][TIMED_PARAM][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['train'] = -1.0
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['sync'] = -1.0
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['test'] = -1.0
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['total'] = -1.0                                
            for metric in self.metrics:
                if not TIMED_PARAM in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][TIMED_PARAM] = {}                
                self.scores[str(method)][dataset][metric][TIMED_PARAM][iFold] = 'error'
            gc.collect()
    
    def getSingleStratifiedSample(self, indices, y, sampleSize):
        if self.bRegression:
            np.random.shuffle(indices)
            return indices[:sampleSize]
        if len(np.unique(y)) == 2:
            return self.getSingleStratifiedSampleBinary(indices, y, sampleSize)
        else:
            return self.getSingleStratifiedSampleMulticlass(indices, y, sampleSize)

    def getSingleStratifiedSampleMulticlass(self, indices, y, sampleSize):
        classes = np.unique(y)
        samples = {}
        overhead = 0
        for c in classes:
            idx = [i for i in indices if y[i] == c]       
            np.random.shuffle(indices)
            size = int(float(len(idx)) / float(len(indices)) * sampleSize)
            if overhead > 0 and size > 1:
                size -= 1                
            if size == 0:
                size = 1
                overhead += 1
            c_sample = indices[:size]
            samples[c] = c_sample             
        s = np.array([]).astype(int)
        for c in classes:
            s = np.append(s, samples[c])
        while len(s) < sampleSize:
            np.random.shuffle(indices)
            id = indices[0]
            if id not in s:
                s = np.append(s, [id])
        s.astype(int)
        return s
  
    def getSingleStratifiedSampleBinary(self, indices, y, sampleSize):
        np.random.shuffle(indices)  
        pos = [i for i in indices if y[i] == 1.0]
        np.random.shuffle(indices)  
        neg = [i for i in indices if y[i] == 0.0]
        if len(pos)+len(neg) != len(indices):
            log( "Error: more classes than 1 and 0 for stratified binary sampling.")
        posSampleSize = int(float(len(pos)) / float(len(indices)) * sampleSize)
        negSampleSize = int(float(len(neg)) / float(len(indices)) * sampleSize)    
        if posSampleSize == 0:
            posSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if negSampleSize > 1:
                    negSampleSize -=1
        if negSampleSize == 0:
            negSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if posSampleSize > 1:
                    posSampleSize -=1
        pos_samples = pos[:posSampleSize]
        neg_samples = neg[:negSampleSize]
        s = np.append(pos_samples,neg_samples)
        while len(s) < sampleSize:
            np.random.shuffle(indices)
            id = indices[0]
            if id not in s:
                s = np.append(s, [id])
        s.astype(int)
        return s    
    
    def runParameterEvaluation(self, metric, folds = 5, dataFrac = 0.1):
        timeString = time.strftime(time_folder_format, time.localtime(time.time()))
        folderPath = os.path.join(self.current_dir, timeString)
        if not os.path.exists(folderPath):
            os.mkdir(folderPath)
        self.exp_dir = folderPath
        #start parameter evaluation
        log("Starting parameter evaluation...", newline=True)
        bestParams = {}
        for dataset in self.datasets:
            bestParams[dataset] = {}
            self.datasetInfo[dataset] = self.dataHandler.getDatasetInfo(dataset)            
            log( "Dataset "+dataset+" "+str(self.datasetInfo[dataset]['task'])+" ("+str(self.datasetInfo[dataset]['N'])+"x"+str(self.datasetInfo[dataset]['D'])+") :")
            X,y = self.dataHandler.getDataset(dataset)   
            newN = len(X)*dataFrac
            sample = self.getSingleStratifiedSample(range(len(X)), y, newN)
            X = X[sample]
            y = y[sample]
            if not self.bRegression:   
                kf = StratifiedKFold(y, folds, shuffle=True) #generate one unused sample, because the last sample has a different number of elements and we throw it away 
            else:
                kf = KFold(len(X), n_folds=folds)
            foldIdxs = [(trainIDX, testIDX) for trainIDX, testIDX in kf]
            method = self.learner[0]
            method.train(X,y)
            w = method.getModel()
            r = w.shape[1] + 2  #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)
            N = int(len(X) / folds)*(folds-1)          
                                             
            for i in xrange(len(self.learner)):                
                bestPerformance = 1e400
                bestParam = None
                for param in self.learner[i].getParamRange():
                    actPerformance = 0.0
                    for iFold in xrange(folds):
                        log( "Fold ",str(iFold),": ", newline = False)
                        trainIDX, testIDX = foldIdxs[iFold]
                        X_train = X[trainIDX]
                        X_test  = X[testIDX]
                        y_train = y[trainIDX]
                        y_test  = y[testIDX] 
                        learner = self.learner[i].__class__(**param)                          
                        learner.train(X_train,y_train)      
                        y_pred = learner.predict(X_test)                                        
                        actPerformance += metric(y_pred, y_test)
                    actPerformance /= float(folds)   
                    if actPerformance < bestPerformance:
                        bestParam = param
                        bestPerformance = actPerformance
                log("Best params for ",learner," are ",bestParam," with ",metric," = ",bestPerformance, newline=True)
                bestParams[dataset][str(learner)] = bestParam
                self.learner[i] = learner.__class__(**bestParam)
        log(bestParams)
        pickle.dump(bestParams, open(self.exp_dir + "/bestParameters.p", 'wb'))

class SparkParallelRadonExperiment(ParallelRadonExperiment):    
    def __init__(self, datasets, dataHandler, learner, metrics, paralllelizationClasses = None, regression = False):             
        ParallelRadonExperiment.__init__(self, datasets, dataHandler, learner, metrics, paralllelizationClasses, regression)        
        
    def run(self, folds = 10, bRunTimedExperiment = True, bOnlyMaxH = False, bCentralOnlyForSpark = False):
        #create experiment subfolder with timestamp
        timeString = time.strftime(time_folder_format, time.localtime(time.time()))
        folderPath = os.path.join(self.current_dir, timeString)
        if not os.path.exists(folderPath):
            os.mkdir(folderPath)
        self.exp_dir = folderPath
        #start experiment
        self.expTimerStart(False)
        for dataset in self.datasets:            
            data = self.dataHandler.getDataset(dataset)
            self.datasetInfo[dataset] = self.dataHandler.getDatasetInfo(dataset)   
            N = self.datasetInfo[dataset]['N']         
            log( "Dataset "+dataset+" "+str(self.datasetInfo[dataset]['task'])+" ("+str(self.datasetInfo[dataset]['N'])+"x"+str(self.datasetInfo[dataset]['D'])+") :")                 
            r = getModelSize(data, self.learner[0], N) + 2 #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)                   
            N = int(N / folds)*(folds-1)
            for iFold in xrange(folds):
                log( "Fold ",str(iFold),": ", newline = False)
                trainData, testData = data.randomSplit([1.0 - 1.0/float(folds), 1.0/float(folds)]) #TODO: check how to do stratified sampling
                trainData.cache()
                testData.cache()
                print trainData.take(5)
                for learner in self.learner:
                    learner = self.checkForOptimalParameters(learner, dataset)
                    self.runCentralExperiment(learner, dataset, trainData, testData, iFold, bCentralOnlyForSpark)                                                                
                    h = 1
                    n = int(float(N) / (float(r)**h))
                    if not bOnlyMaxH:
                        while n >= 2:                                    
                            h = int(math.log(float(N)/float(n))/math.log(float(r))) #floor of that number 
                            for paraClass in self.paralllelizationClasses:
                                if paraClass.__name__ == "SparkRadonBoostOnLearnerSet":
                                    continue
                                method = paraClass(method = learner.__class__(**learner.getInitParams()))
                                _ = self.runParallelExperiment(method, dataset, trainData, testData, iFold, n, h)
                                #if 'RadonBoost' in str(method) and bRunTimedExperiment:
                                #    self.runCentralExperimentSameTime(learner, dataset, X_train, y_train, X_test, y_test, iFold, trainduration, n, h)      
                            gc.collect()     
                            h += 1
                            n = int(float(N) / (float(r)**h))   
                    else:
                        while n >= 2:                                    
                            h = int(math.log(float(N)/float(n))/math.log(float(r))) #floor of that number                               
                            n = int(float(N) / (float(r)**(h+1)))   
                        log("Max h = ",h,newline=True)
                        for paraClass in self.paralllelizationClasses:
                            if paraClass.__name__ == "SparkRadonBoostOnLearnerSet":
                                continue
                            method = paraClass(method = learner.__class__(**learner.getInitParams()))
                            _ = self.runParallelExperiment(method, dataset, trainData, testData, iFold, n, h)
                for paraClass in self.paralllelizationClasses:
                    if paraClass.__name__ == "SparkRadonBoostOnLearnerSet":
                        h = 1
                        n = int(float(N) / (float(r)**h))
                        if not bOnlyMaxH:
                            while n >= 2:                                    
                                h = int(math.log(float(N)/float(n))/math.log(float(r))) #floor of that number 
                                method = paraClass(methods= self.learner)
                                self.runParallelExperimentOnLearnerSet(method, dataset, trainData, testData, iFold, n, h)
                                gc.collect()     
                                h += 1
                                n = int(float(N) / (float(r)**h))   
                        else:
                            while n >= 2:                                    
                                h = int(math.log(float(N)/float(n))/math.log(float(r))) #floor of that number                               
                                n = int(float(N) / (float(r)**(h+1)))   
                            log("Max h = ",h,newline=True)
                            method = paraClass(methods= self.learner)
                            self.runParallelExperimentOnLearnerSet(method, dataset, trainData, testData, iFold, n, h)
                    gc.collect()     
                gc.collect()
                log( "done.")
            gc.collect()     
                            
        self.expTimerStop(False)                                                                                  
        out = Output(self.exp_dir)
        out.writeParallelExperimentSummary(self.scores, self.durations, self.datasetInfo, self.methods, self.learner, self.radonLearner, self.metrics, self.startTime, self.endTime, folds, self.actualNForTimedCentralExps)
        out.createResultPlotterFile()
        log( str(self.scores))
        log( str(self.durations))
        log( str(self.datasetInfo))
                
    def runCentralExperiment(self, method, dataset, trainData, testData, iFold, bCentralOnlyForSpark = False):
        if not CENTRAL in self.durations[str(method)][dataset]: 
            self.durations[str(method)][dataset][CENTRAL] = {}
        if not iFold in self.durations[str(method)][dataset][CENTRAL]: 
            self.durations[str(method)][dataset][CENTRAL][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
        log( str(method) + " ... ", newline = False)     
        try:    
            trainduration = 0.0   
            testduration = 0.0  
            fullstart = datetime.now() 
            if method.isSpark:
                trainduration, _ = method.train(trainData)
                td = testData.map(lambda lp: lp.features)
                start = datetime.now()
                y_pred = np.array(method.predict(td).collect())
                y_true = np.array(testData.map(lambda lp: lp.label).collect())
                stop = datetime.now()
                testduration = (stop - start).total_seconds()
            else:           
                if bCentralOnlyForSpark:
                    testduration = 0.0
                    y_pred = [1.0, -1.0]
                    y_true = [-1.0, 1.0]
                    trainduration = {'train':0.0, 'sync':0.0}
                else:
                    X = np.array(trainData.map(lambda x: x.features.toArray()).collect())
                    y = np.array(trainData.map(lambda x: x.label).collect())
                    trainduration, _ = method.train(X,y)      
                    X_test = np.array(testData.map(lambda lp: lp.features.toArray()).collect())
                    start = datetime.now()
                    y_pred = method.predict(X_test)
                    y_true = np.array(testData.map(lambda lp: lp.label).collect())
                    stop = datetime.now()
                    testduration = (stop - start).total_seconds()   
            fullstop = datetime.now()                            
            self.durations[str(method)][dataset][CENTRAL][iFold]['train'] = trainduration['train']
            self.durations[str(method)][dataset][CENTRAL][iFold]['sync'] = trainduration['sync']
            self.durations[str(method)][dataset][CENTRAL][iFold]['test'] = testduration
            self.durations[str(method)][dataset][CENTRAL][iFold]['total'] = trainduration['train']+trainduration['sync']+testduration          
            self.durations[str(method)][dataset][CENTRAL][iFold]['full'] = (fullstop - fullstart).total_seconds()                         
            for metric in self.metrics:
                if not CENTRAL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][CENTRAL] = {}
                self.scores[str(method)][dataset][metric][CENTRAL][iFold] = metric(y_pred, y_true)      
            gc.collect()
        except:
            log( "error!", sys.exc_info()[0])
            self.durations[str(method)][dataset][CENTRAL][iFold]['train'] = -1.0
            self.durations[str(method)][dataset][CENTRAL][iFold]['sync'] = -1.0
            self.durations[str(method)][dataset][CENTRAL][iFold]['test'] = -1.0
            self.durations[str(method)][dataset][CENTRAL][iFold]['total'] = -1.0                                
            for metric in self.metrics:
                if not CENTRAL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][CENTRAL] = {}
                self.scores[str(method)][dataset][metric][CENTRAL][iFold] = 'error'
            gc.collect()         
            
    def runParallelExperiment(self, method, dataset, trainData, testData, iFold, n, h):        
        PARALLEL = (n,h)        
#         if "MinSample" in str(method):
#             method.train(data)
#             w = method.getModel()
#             R = rc.getRadonNumber(w)
#             min_n = len(method.getMinimalSample(range(len(ShuffledX)), ShuffledY, int(R**h))[0])                    
        if not PARALLEL in self.durations[str(method)][dataset]: 
            self.durations[str(method)][dataset][PARALLEL] = {}
        if not iFold in self.durations[str(method)][dataset][PARALLEL]: 
            self.durations[str(method)][dataset][PARALLEL][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
        log( str(method) + "(h=" + str(h) + ") ... ", newline = False)     
        try:            
            if NOPARALLELSPARK and method.learner.isSpark:
                for metric in self.metrics:
                    if not PARALLEL in self.scores[str(method)][dataset][metric]:
                        self.scores[str(method)][dataset][metric][PARALLEL] = {}
                    self.scores[str(method)][dataset][metric][PARALLEL][iFold] = -1.0
                return 0.0
            fullstart = datetime.now()
            trainduration, _ = method.train(trainData, bRegression = self.bRegression)
            td = testData.map(lambda lp: lp.features)
            start = datetime.now()
            y_pred = np.array(method.predict(td))
            y_true = np.array(testData.map(lambda lp: lp.label).collect())
            stop = datetime.now()
            fullstop = datetime.now()
            testduration = (stop - start).total_seconds()
            self.durations[str(method)][dataset][PARALLEL][iFold]['train'] = trainduration['train']
            self.durations[str(method)][dataset][PARALLEL][iFold]['sync'] = trainduration['sync']
            self.durations[str(method)][dataset][PARALLEL][iFold]['test'] = testduration
            self.durations[str(method)][dataset][PARALLEL][iFold]['total'] = trainduration['train']+trainduration['sync']+testduration              
            self.durations[str(method)][dataset][PARALLEL][iFold]['full'] = (fullstop - fullstart).total_seconds()                 
            for metric in self.metrics:
                if not PARALLEL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][PARALLEL] = {}
                self.scores[str(method)][dataset][metric][PARALLEL][iFold] = metric(y_pred, y_true)
    #             if "MinSample" in str(method): 
    #                 MIN_SAMPLE = "MinSample"+str(PARALLEL)  
    #                 if MIN_SAMPLE not in self.actualNForTimedCentralExps[str(method)][dataset]:
    #                     self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE] = {}
    #                 self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE][iFold] = min_n
                gc.collect()     
        except:
            log( "error: ", sys.exc_info()[0])
            self.durations[str(method)][dataset][PARALLEL][iFold]['train'] = -1.0
            self.durations[str(method)][dataset][PARALLEL][iFold]['sync'] = -1.0
            self.durations[str(method)][dataset][PARALLEL][iFold]['test'] = -1.0
            self.durations[str(method)][dataset][PARALLEL][iFold]['total'] = -1.0                                
            for metric in self.metrics:
                if not PARALLEL in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][PARALLEL] = {}
                self.scores[str(method)][dataset][metric][PARALLEL][iFold] = float('NaN')
            if "MinSample" in str(method): 
                MIN_SAMPLE = "MinSample"+str(PARALLEL)  
                if MIN_SAMPLE not in self.actualNForTimedCentralExps[str(method)][dataset]:
                    self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE] = {}
                self.actualNForTimedCentralExps[str(method)][dataset][MIN_SAMPLE][iFold] = MIN_SAMPLE
            gc.collect()   
        return self.durations[str(method)][dataset][PARALLEL][iFold]['train']+self.durations[str(method)][dataset][PARALLEL][iFold]['sync']
    
    def runParallelExperimentOnLearnerSet(self, method, dataset, trainData, testData, iFold, n, h):        
        PARALLEL = (n,h)                                 
        try:            
            fullstart = datetime.now()
            traindurations, _ = method.train(trainData, bRegression = self.bRegression)
            td = testData.map(lambda lp: lp.features)
            start = datetime.now()
            predictions = method.predict(td)
            y_true = np.array(testData.map(lambda lp: lp.label).collect())
            stop = datetime.now()
            fullstop = datetime.now()
            testduration = (stop - start).total_seconds() / float(len(method.learners))
            for learner in method.learners:
                methodName = method.getExpIdentifier(learner)
                if methodName not in self.radonLearner:
                    self.radonLearner.append(methodName)
                if not methodName in self.durations:
                    self.durations[methodName] = {}
                if not dataset in self.durations[methodName]:
                    self.durations[methodName][dataset] = {}
                if not PARALLEL in self.durations[methodName][dataset]: 
                    self.durations[methodName][dataset][PARALLEL] = {}
                if not iFold in self.durations[methodName][dataset][PARALLEL]: 
                    self.durations[methodName][dataset][PARALLEL][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
                log( methodName + "(h=" + str(h) + ") ... ", newline = False)     
                
                y_pred = np.array(predictions[learner])
                
                self.durations[methodName][dataset][PARALLEL][iFold]['train'] = traindurations[learner]['train']
                self.durations[methodName][dataset][PARALLEL][iFold]['sync'] = traindurations[learner]['sync']
                self.durations[methodName][dataset][PARALLEL][iFold]['test'] = testduration
                self.durations[methodName][dataset][PARALLEL][iFold]['total'] = traindurations[learner]['train']+traindurations[learner]['sync']+testduration              
                self.durations[methodName][dataset][PARALLEL][iFold]['full'] = (fullstop - fullstart).total_seconds()                 
                for metric in self.metrics:
                    if not methodName in self.scores:
                        self.scores[methodName] = {}
                    if not dataset in self.scores[methodName]:
                        self.scores[methodName][dataset] = {}
                    if not metric in self.scores[methodName][dataset]:
                        self.scores[methodName][dataset][metric] = {}
                    if not PARALLEL in self.scores[methodName][dataset][metric]:
                        self.scores[methodName][dataset][metric][PARALLEL] = {}                    
                    self.scores[methodName][dataset][metric][PARALLEL][iFold] = metric(y_pred, y_true)
                    gc.collect()     
        except:
            log( "error: ", sys.exc_info()[0])
            for learner in method.learners:
                methodName = method.getExpIdentifier(learner)
                if not methodName in self.durations:
                    self.durations[methodName] = {}
                if not dataset in self.durations[methodName]:
                    self.durations[methodName][dataset] = {}
                if not PARALLEL in self.durations[methodName][dataset]: 
                    self.durations[methodName][dataset][PARALLEL] = {}
                if not iFold in self.durations[methodName][dataset][PARALLEL]: 
                    self.durations[methodName][dataset][PARALLEL][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
                self.durations[methodName][dataset][PARALLEL][iFold]['train'] = -1.0
                self.durations[methodName][dataset][PARALLEL][iFold]['sync'] = -1.0
                self.durations[methodName][dataset][PARALLEL][iFold]['test'] = -1.0
                self.durations[methodName][dataset][PARALLEL][iFold]['total'] = -1.0
                self.durations[methodName][dataset][PARALLEL][iFold]['full'] = -1.0   
                for metric in self.metrics:
                    if not methodName in self.scores:
                        self.scores[methodName] = {}
                    if not dataset in self.scores[methodName]:
                        self.scores[methodName][dataset] = {}
                    if not metric in self.scores[methodName][dataset]:
                        self.scores[methodName][dataset][metric] = {}
                    if not PARALLEL in self.scores[methodName][dataset][metric]:
                        self.scores[methodName][dataset][metric][PARALLEL] = {}                    
                    self.scores[methodName][dataset][metric][PARALLEL][iFold] = float('NaN')
    
       
    def runCentralExperimentSameTime(self, method, dataset, X_train, y_train, X_test, y_test, iFold, traindurationParallel, nParallel, h):        
        log( str(method),"Timed... ", newline = False)     
        try:       
            PARALLEL = (nParallel,h)
            TIMED_PARAM = TIMED + str(PARALLEL)   
            nOld = -1
            trainduration = None
            N = len(X_train)
            n_min = len(np.unique(y_train))               
            min = n_min
            max = N  
            n = nParallel
            indices = np.arange(N)
            np.random.shuffle(indices)
            #sample = self.getSingleStratifiedSample(indices, y_train, n)
            sample = indices[:n]
            while len(np.unique(y_train[sample])) != len(np.unique(y_train)): #make this more efficient
                    log( "Error: stratified sampling went wrong...retry")
                    np.random.shuffle(indices)
                    sample = self.getSingleStratifiedSample(indices, y_train, n)
            if len(sample) != n:
                log( "Error: n should be ",n," but it's ",str(len(sample)),". Dajum!\n")
            X = X_train[sample]
            y = y_train[sample]   
            trainduration, = method.train(X, y)
            start = datetime.now()
            y_pred = method.predict(X_test)
            stop = datetime.now()
            testduration = (stop - start).total_seconds()            
            if not TIMED_PARAM in self.durations[str(method)][dataset]: 
                self.durations[str(method)][dataset][TIMED_PARAM] = {}            
            if not iFold in self.durations[str(method)][dataset][TIMED_PARAM]: 
                self.durations[str(method)][dataset][TIMED_PARAM][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['train'] = trainduration['train']
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['sync'] = trainduration['sync']
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['test'] = testduration
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['total'] = trainduration['train']+trainduration['sync']+testduration                                
            for metric in self.metrics:
                if not TIMED_PARAM in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][TIMED_PARAM] = {}
                self.scores[str(method)][dataset][metric][TIMED_PARAM][iFold] = metric(y_pred, y_test)
            if TIMED_PARAM not in self.actualNForTimedCentralExps[str(method)][dataset]:
                self.actualNForTimedCentralExps[str(method)][dataset][TIMED_PARAM] = {}
            self.actualNForTimedCentralExps[str(method)][dataset][TIMED_PARAM][iFold] = n
            gc.collect()     
        except:
            log( "error!", sys.exc_info()[0])
            if not TIMED_PARAM in self.durations[str(method)][dataset]: 
                self.durations[str(method)][dataset][TIMED_PARAM] = {}            
            if not iFold in self.durations[str(method)][dataset][TIMED_PARAM]: 
                self.durations[str(method)][dataset][TIMED_PARAM][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['train'] = -1.0
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['sync'] = -1.0
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['test'] = -1.0
            self.durations[str(method)][dataset][TIMED_PARAM][iFold]['total'] = -1.0                                
            for metric in self.metrics:
                if not TIMED_PARAM in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][TIMED_PARAM] = {}                
                self.scores[str(method)][dataset][metric][TIMED_PARAM][iFold] = 'error'
            gc.collect()
    
    def getSingleStratifiedSample(self, indices, y, sampleSize):
        if self.bRegression:
            np.random.shuffle(indices)
            return indices[:sampleSize]
        if len(np.unique(y)) == 2:
            return self.getSingleStratifiedSampleBinary(indices, y, sampleSize)
        else:
            return self.getSingleStratifiedSampleMulticlass(indices, y, sampleSize)

    def getSingleStratifiedSampleMulticlass(self, indices, y, sampleSize):
        classes = np.unique(y)
        samples = {}
        overhead = 0
        for c in classes:
            idx = [i for i in indices if y[i] == c]       
            np.random.shuffle(indices)
            size = int(float(len(idx)) / float(len(indices)) * sampleSize)
            if overhead > 0 and size > 1:
                size -= 1                
            if size == 0:
                size = 1
                overhead += 1
            c_sample = indices[:size]
            samples[c] = c_sample             
        s = np.array([]).astype(int)
        for c in classes:
            s = np.append(s, samples[c])
        while len(s) < sampleSize:
            np.random.shuffle(indices)
            id = indices[0]
            if id not in s:
                s = np.append(s, [id])
        s.astype(int)
        return s
  
    def getSingleStratifiedSampleBinary(self, indices, y, sampleSize):
        np.random.shuffle(indices)  
        pos = [i for i in indices if y[i] == 1.0]
        np.random.shuffle(indices)  
        neg = [i for i in indices if y[i] == 0.0]
        if len(pos)+len(neg) != len(indices):
            log( "Error: more classes than 1 and 0 for stratified binary sampling.")
        posSampleSize = int(float(len(pos)) / float(len(indices)) * sampleSize)
        negSampleSize = int(float(len(neg)) / float(len(indices)) * sampleSize)    
        if posSampleSize == 0:
            posSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if negSampleSize > 1:
                    negSampleSize -=1
        if negSampleSize == 0:
            negSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if posSampleSize > 1:
                    posSampleSize -=1
        pos_samples = pos[:posSampleSize]
        neg_samples = neg[:negSampleSize]
        s = np.append(pos_samples,neg_samples)
        while len(s) < sampleSize:
            np.random.shuffle(indices)
            id = indices[0]
            if id not in s:
                s = np.append(s, [id])
        s.astype(int)
        return s    
    
    def checkForOptimalParameters(self, learner, dataset):
        if os.path.exists("bestParameters.p"):
            log("Loading optimal parameters...")
            bestParameters = pickle.load(open("bestParameters.p", 'r'))
            if dataset in bestParameters and str(learner) in bestParameters[dataset]:
                params = bestParameters[dataset][str(learner)]
                learner = learner.__class__(**params)
            log("done.")
            return learner
        else:
            return learner
    
    def runParameterEvaluation(self, metric, folds = 5, dataFrac = 0.1):
        timeString = time.strftime(time_folder_format, time.localtime(time.time()))
        folderPath = os.path.join(self.current_dir, timeString)
        if not os.path.exists(folderPath):
            os.mkdir(folderPath)
        self.exp_dir = folderPath
        #start parameter evaluation
        log("Starting parameter evaluation...", newline=True)
        for dataset in self.datasets:
            self.datasetInfo[dataset] = self.dataHandler.getDatasetInfo(dataset)
            N = self.datasetInfo[dataset]['N']     
            log( "Dataset "+dataset+" "+str(self.datasetInfo[dataset]['task'])+" ("+str(self.datasetInfo[dataset]['N'])+"x"+str(self.datasetInfo[dataset]['D'])+") :")
            data = self.dataHandler.getDataset(dataset)                 
            r = getModelSize(data, self.learner[0], N) + 2 #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)                   
            data, _ = data.randomSplit([dataFrac, 1.0-dataFrac])
            N = int(N / folds)*(folds-1)
            for i in xrange(len(self.learner)):                
                bestPerformance = 1e400
                bestParam = None
                for param in self.learner[i].getParamRange():
                    actPerformance = 0.0
                    for iFold in xrange(folds):
                        log( "Fold ",str(iFold),": ", newline = False)
                        trainData, testData = data.randomSplit([1.0 - 1.0/float(folds), 1.0/float(folds)]) #TODO: check how to do stratified sampling
                        learner = self.learner[i].__class__(**param)                
                        if learner.isSpark:
                            learner.train(trainData)
                            td = testData.map(lambda lp: lp.features)
                            y_pred = np.array(learner.predict(td).collect())
                            y_true = np.array(testData.map(lambda lp: lp.label).collect())
                        else:           
                            X = np.array(trainData.map(lambda x: x.features.toArray()).collect())
                            y = np.array(trainData.map(lambda x: x.label).collect())
                            learner.train(X,y)      
                            X_test = np.array(testData.map(lambda lp: lp.features.toArray()).collect())
                            y_pred = learner.predict(X_test)
                            y_true = np.array(testData.map(lambda lp: lp.label).collect())                                             
                            actPerformance += metric(y_pred, y_true)
                    actPerformance /= float(folds)   
                    if actPerformance < bestPerformance:
                        bestParam = param
                        bestPerformance = actPerformance
                log("Best params for ",learner," are ",bestParam," with ",metric," = ",bestPerformance, newline=True)
                self.learner[i] = learner.__class__(**bestParam)
                
                
class OldParallelRadonExperiment(Experiment):
    def __init__(self, datasets, learner, metrics):        
        self.dataHandler = Datasets()
        self.datasets = datasets
        self.learner = learner
        self.radonLearner = []
        self.methods = []
        for l in learner:
            self.methods.append(l)
            r = RadonBoost(method = l.__class__())
            self.radonLearner.append(r)
            self.methods.append(r)
        self.metrics = metrics
        self.scores = {}
        self.durations = {}
        self.datasetInfo = {}
        for method in self.methods:
            self.scores[str(method)] = {}
            self.durations[str(method)] = {}
            for dataset in datasets:
                self.scores[str(method)][dataset] = {}
                self.durations[str(method)][dataset] = {}
                for metric in self.metrics:
                    self.scores[str(method)][dataset][metric] = {}
        self.current_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
     
     
    def run(self, folds = 10, sampleFunction = sampleSizeLinear, steps = 10):
        #create experiment subfolder with timestamp
        timeString = time.strftime(time_folder_format, time.localtime(time.time()))
        folderPath = os.path.join(self.current_dir, timeString)
        os.mkdir(folderPath)
        self.exp_dir = folderPath
        #start experiment
        self.expTimerStart(False)
        if len(self.learner) != len(self.radonLearner):
            log( "Error: different number of methods and parallelized methods")
        for dataset in self.datasets:
            self.datasetInfo[dataset] = self.dataHandler.getDatasetInfo(dataset)            
            log( "Dataset "+dataset+" "+str(self.datasetInfo[dataset]['task'])+" ("+str(self.datasetInfo[dataset]['N'])+"x"+str(self.datasetInfo[dataset]['D'])+") :")
            X,y = self.dataHandler.getDataset(dataset)
            for iFold in xrange(folds):
                log( "Fold ",str(iFold),": ", newline = False)
                X_train, X_test, y_train, y_test = cross_validation.train_test_split(X,y, test_size = 1./float(folds))
                sampleSetSizes = sampleFunction(len(X_train), steps)             
                for n in sampleSetSizes:                                        
                    I = range(len(X_train))
                    random.shuffle(I)
                    idx = np.array(I)
                    SampleX = np.array(X_train)[idx]
                    SampleY = np.array(y_train)[idx]
                    N = len(SampleX)
                    r = len(SampleX[0])+2
                    #n_min = radonBoosting.getOptimalSampleSize(N, r)                                                            
                    for method in self.methods:
                        hmax = int(math.log(float(N)/float(n))/math.log(float(r))) #floor of that number          
                        for h in xrange(0,hmax+1):   
                            self.runSingleExperiment(method, dataset, SampleX, SampleY, X_test, y_test, n, h, iFold) 
                            #store experiment results and durations
                        gc.collect()     
                    gc.collect()     
                gc.collect()
                log( "done.")
            gc.collect()     
                             
        self.expTimerStop(False)                                                                                  
        out = Output(self.exp_dir)
        out.writeOldParallelExperimentSummary(self.scores, self.durations, self.datasetInfo, self.methods, self.metrics, self.startTime, self.endTime, folds)
        out.createResultPlotterFile()
        log( self.scores)
        log( self.durations)
        log( self.datasetInfo)
                 
    def runSingleExperiment(self, method, dataset, SampleX, SampleY, X_test, y_test, n, h, iFold):
        if not n in self.durations[str(method)][dataset]: 
            self.durations[str(method)][dataset][n] = {}
        if not h in self.durations[str(method)][dataset][n]: 
            self.durations[str(method)][dataset][n][h] = {}
        if not iFold in self.durations[str(method)][dataset][n][h]: 
            self.durations[str(method)][dataset][n][h][iFold] = {'train':0.0, 'test':0.0, 'sync':0.0,'total':0.0}
        log( str(method) + " ... ", newline = False)     
        try:     
            if hasattr(method, 'h'):
                method.h = h
            if hasattr(method, 'minSampleSize'):
                method.minSampleSize = n              
            trainduration, = method.train(SampleX, SampleY)
            start = datetime.now()
            y_pred = method.predict(X_test)
            stop = datetime.now()
            testduration = (stop - start).total_seconds()
            self.durations[str(method)][dataset][n][h][iFold]['train'] = trainduration['train']
            self.durations[str(method)][dataset][n][h][iFold]['sync'] = trainduration['sync']
            self.durations[str(method)][dataset][n][h][iFold]['test'] = testduration
            self.durations[str(method)][dataset][n][h][iFold]['total'] = trainduration['train']+trainduration['sync']+testduration                                
            for metric in self.metrics:
                if not n in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][n] = {}
                if not h in self.scores[str(method)][dataset][metric][n]:
                    self.scores[str(method)][dataset][metric][n][h] = {}
                self.scores[str(method)][dataset][metric][n][h][iFold] = metric(y_pred, y_test)      
            gc.collect()     
        except:
            log( "error!", sys.exc_info()[0])
            self.durations[str(method)][dataset][n][h][iFold]['train'] = -1.0
            self.durations[str(method)][dataset][n][h][iFold]['sync'] = -1.0
            self.durations[str(method)][dataset][n][h][iFold]['test'] = -1.0
            self.durations[str(method)][dataset][n][h][iFold]['total'] = -1.0                                
            for metric in self.metrics:
                if not n in self.scores[str(method)][dataset][metric]:
                    self.scores[str(method)][dataset][metric][n] = {}
                if not h in self.scores[str(method)][dataset][metric][n]:
                    self.scores[str(method)][dataset][metric][n][h] = {}
                self.scores[str(method)][dataset][metric][n][h][iFold] = 'error'
            gc.collect()       

def getModelSize(data, method, N = None):
    size = 100.0
    w = None
    if N == None:
        N = data.count()
    if method.isSpark:
        fraction = size / float(N)
        smallSample = data.sample(withReplacement=False, fraction = fraction).cache()  
        method.train(smallSample)
        w = method.getModel()
    else:
        fraction = size / float(N)
        smallSample = data.sample(withReplacement=False, fraction = fraction).cache()    
        X = np.array(smallSample.map(lambda p: p.features.toArray()).collect())
        y = np.array(smallSample.map(lambda p: p.label).collect())
        method.train(X,y)
        w = method.getModel()
    return w.shape[1]
             
class ClassicalExperiment(Experiment):    
    def run(self, nFolds = 10):
        #create experiment subfolder with timestamp
        startTime = self.expTimerStart()
        timeString = time.strftime(time_folder_format, time.localtime(startTime))
        folderPath = os.path.join(self.current_dir, timeString)
        os.mkdir(folderPath)
        self.exp_dir = folderPath
        #loop over all datasets and methods and run experiments
        for dataset in self.datasets:
            self.datasetInfo[dataset] = self.dataHandler.getDatasetInfo(dataset)            
            log( "Dataset "+dataset+" "+str(self.datasetInfo[dataset]['task'])+" ("+str(self.datasetInfo[dataset]['N'])+"x"+str(self.datasetInfo[dataset]['D'])+") :")
            X,y = self.dataHandler.getDataset(dataset)            
            for i in xrange(nFolds):
                log( "Fold "+str(i+1)+" method: ", newline = False)
                X_train, X_test, y_train, y_test = cross_validation.train_test_split(X,y, test_size = 1.0/float(nFolds))
                for method in self.methods:
                    log( str(method) + " ... ", newline = False)
                    method.determineSampleSize(X_train)
                    start = datetime.now()
                    method.train(X_train, y_train)
                    y_pred = method.predict(X_test)
                    stop = datetime.now()
                    duration = (stop - start).total_seconds()
                    #log( str(start) + " " + str(stop) + " duration: " + str(duration))
                    self.durations[str(method)][dataset] += duration / float(nFolds)
                    for metric in self.metrics:
                        self.scores[str(method)][dataset][metric] += (metric(y_pred, y_test)  / float(nFolds))
                    gc.collect()                 
                gc.collect()
            log( "done." )
            gc.collect()
        endTime = self.expTimerStop(startTime)
        out = Output(self.exp_dir)
        out.writeExperimentSummary(self.scores, self.durations, self.datasetInfo, self.methods, self.metrics, startTime, endTime, nFolds)        
        log( self.scores)
        log( self.durations)
        log( self.datasetInfo)