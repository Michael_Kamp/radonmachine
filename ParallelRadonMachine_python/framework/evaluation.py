#import numpy as np
from sklearn import metrics
import math
import numpy as np
from framework.logger import log

class Metric:
    def __init__(self):
        pass
    
    def __str__(self):
        return "gen_metric"
    
    def __call__(self, y_pred, y_true):
        return -1.0

class AUC(Metric):
    def __init__(self, average = "macro", sampleWeight = None):
        self.average        = average
        self.sampleWeight   = sampleWeight
        pass
    
    def __str__(self):
        return "AUC"

    def __call__(self, y_pred, y_true):
        return metrics.roc_auc_score(y_true, y_pred, average = self.average, sample_weight = self.sampleWeight)
        
class ACC(Metric):
    def __init__(self):
        pass
    
    def __str__(self):
        return "ACC"
    
    def __call__(self, y_pred, y_true):
        count = 0.0
        for i in xrange(len(y_pred)):
            if y_pred[i] == y_true[i]:
                count += 1.0
        if len(y_pred) > 0:            
            return count / float(len(y_pred))
        else:
            log( "Error in metric: y_pred = ",y_pred," y_true = ",y_true)
            return 0.0
        
class RMSE(Metric):
    def __init__(self):
        pass
    
    def __str__(self):
        return "RMSE"
    
    def __call__(self, y_pred, y_true):
        MSE = 0.0
        for i in xrange(len(y_pred)):
            SE = (y_pred[i] - y_true[i])**2
            MSE += SE         
        if len(y_pred) < 1:  
            log( "Error in metric: y_pred = ",y_pred," y_true = ",y_true)
            return 0.0          
        MSE /= float(len(y_pred))
        RMSE = math.sqrt(MSE)
        return RMSE
    
class MircoAveragedFmeasure(Metric):
    def __init__(self):
        pass
    
    def __str__(self):
        return "MircoAveragedF-Measure"
    
    def __call__(self, y_pred, y_true):
        classes = np.unique(y_true)
        TPi = {}
        FPi = {}
        FNi = {}
        for c in classes:
            TPi[c] = 0.0
            FPi[c] = 0.0
            FNi[c] = 0.0
            for i in xrange(len(y_true)):
                if y_true[i] == c:
                    if y_pred[i] == y_true[i]:
                        TPi[c] += 1.0
                    else:
                        FNi[c] += 1.0
                elif y_pred[i] == c:
                    FPi[c] += 1.0
        sumTP = 0.0
        sumTPFP = 0.0
        sumTPFN = 0.0
        for c in classes:
            sumTP += TPi[c]
            sumTPFP += TPi[c] + FPi[c]
            sumTPFN += TPi[c] + FNi[c]
        PREC = sumTP / sumTPFP
        REC  = sumTP / sumTPFN
        Fmeasure = 0.0
        if (PREC + REC) > 0:            
            Fmeasure = (2*PREC*REC) / (PREC + REC)
            return Fmeasure
        else:
            log( "Error in metric: y_pred = ",y_pred," y_true = ",y_true)
            return 0.0

class MacroAveragedFmeasure(Metric):
    def __init__(self):
        pass
    
    def __str__(self):
        return "MacroAveragedF-Measure"
    
    def __call__(self, y_pred, y_true):
        classes = np.unique(y_true)
        Fi = {}
        try:
            for c in classes:            
                TP = 0.0
                FP = 0.0
                FN = 0.0
                for i in xrange(len(y_true)):
                    if y_true[i] == c:
                        if y_pred[i] == y_true[i]:
                            TP += 1.0
                        else:
                            FN += 1.0
                    elif y_pred[i] == c:
                        FP += 1.0                
                PREC = 1.0
                if TP + FP > 0.0:
                    PREC = TP / (TP + FP)
                REC  = 1.0
                if TP + FN > 0.0:
                    REC = TP / (TP + FN)
                Fi[c] = (2*PREC*REC) / (PREC + REC)
            Fmeasure = 0.0
            M = float(len(classes))
            for c in classes:
                Fmeasure += Fi[c]
            Fmeasure /= M
            return Fmeasure
        except:
            log( "Error in metric: y_pred = ",y_pred," y_true = ",y_true)
            return 0.0      