from datetime import datetime
import numpy as np
from spark import SparkLearner
from pyspark.mllib.classification import SVMWithSGD
from pyspark.mllib.classification import LogisticRegressionWithLBFGS
from pyspark.mllib.linalg import _convert_to_vector
#from scipy.special.basic import lmbda
from framework.logger import log

class SparkLinearClassifier(SparkLearner):
    def __init__(self):
        SparkLearner.__init__(self)
        self.identifier = "GenericSparkLinearClassifier (please replace)"
        self.model = None
        self.learner = None
    
    def getInitParams(self):
        return {}
    
    def getTrainParams(self):
        return self.getInitParams()
        
    def train(self, data):
        start = datetime.now()        
        self.model = self.learner.train(data, **self.getTrainParams())
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()
    
    def predict(self, data):
        return self.model.predict(data)
    
    def getModel(self):
        return np.array([self.model.weights.toArray().tolist() + [self.model.intercept]])
    
    def setModel(self, model):                
        oldModel = self.getModel()
        if model.shape != oldModel.shape:
            log( "Error: new model shape does not fit current model shape. ", newline = False)
            log( "Expected: "+str(self.learner.coef_.shape)+" but got "+str(model.shape)+".")
        else:
            weights = _convert_to_vector(model[0][:-1])
            intercept = model[0][-1]
            self.model._coeff = weights
            self.model._intercept = intercept

class SparkParallelSGD(SparkLinearClassifier):
    def __init__(self, iterations=100, step=1.0, regParam=0.01, miniBatchFraction=1.0, initialWeights=None, regType="l2", intercept=False, validateData=True):
        SparkLinearClassifier.__init__(self)
        self.identifier = "SparkParallelSGD"
        self.iterations         = iterations
        self.step               = step
        self.regParam           = regParam
        self.miniBatchFraction  = miniBatchFraction
        self.initialWeights     = initialWeights
        self.regType            = regType
        self.intercept          = intercept
        self.validateData       = validateData
        self.learner = SVMWithSGD
        
    def getInitParams(self):
        return {'iterations':self.iterations, 'step':self.step, 'regParam':self.regParam, 'miniBatchFraction':self.miniBatchFraction, 'initialWeights':self.initialWeights, 'regType':self.regType, 'intercept':self.intercept, 'validateData':self.validateData}    
    
    def getTrainParams(self):
        return {}
    
class SparkLogisticRegressionlBFGS(SparkLinearClassifier):
    def __init__(self, iterations=100, regParam=0.01, initialWeights=None, regType="l2", intercept=False, validateData=True):
        SparkLinearClassifier.__init__(self)
        self.identifier = "SparkLogisticRegressionlBFGS"
        self.iterations         = iterations
        self.regParam           = regParam
        self.initialWeights     = initialWeights
        self.regType            = regType
        self.intercept          = intercept
        self.validateData       = validateData
        self.learner = LogisticRegressionWithLBFGS
        
    def getInitParams(self):
        return {'iterations':self.iterations, 'regParam':self.regParam, 'initialWeights':self.initialWeights, 'regType':self.regType, 'intercept':self.intercept, 'validateData':self.validateData,}
    
    def getTrainParams(self):
        params = self.getInitParams()
        params['numClasses'] = 2
        return params