import sys

import findspark
findspark.init()

from spark.sparkDatasets import SparkDatasets
from framework.evaluation import ACC, AUC
from framework.experiment import SparkParallelRadonExperiment
from spark.classification import SparkParallelSGD as SparkParallelSGD
from spark.classification import SparkLogisticRegressionlBFGS as SparkLogReglBFGS
from learner.classification import LinearSVC, LogisticRegressionNewtonCG, LogisticRegressionLBFGS, StochasticGradientDescent
from spark.radonBoosting import SparkRadonBoost as sRB

learner = [LinearSVC(C = 1.0), LogisticRegressionNewtonCG(C = 1.0), LogisticRegressionLBFGS(C = 1.0), StochasticGradientDescent(), SparkParallelSGD(regParam = 1.0), SparkLogReglBFGS(regParam = 1.0), ]
metrics = [ACC(), AUC()]
paralllelizationClasses = [sRB]

dataHandler = SparkDatasets()
datasets = dataHandler.getDatasetNames("sparkDatasets")

experiment = SparkParallelRadonExperiment(datasets, dataHandler, learner, metrics, paralllelizationClasses)
experiment.run(folds=10, bOnlyMaxH=True)