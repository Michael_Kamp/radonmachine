from decimal import Decimal, localcontext
import math
from datetime import datetime
from learner import Learner
import numpy as np
import radonComputation as rc
from framework.logger import log

MINSAMPLESIZE = 30

OPT_SAMPLE_SIZES = {(349,11):58,(184,9):41,(345,8):34,(500,9):41,(135,15):98,(77,21):76,(9509,11):58,(1886,9):41,(217,18):133,(629,11):49,(331,9):35,(621,8):34,(900,9):41,(243,15):98,(139,21):138,(17117,11):58,(3394,9):41,(391,18):133,(692,11):58,(364,9):35,(683,8):34,(990,9):41,(267,15):98,(153,21):152,(18828,11):58,(3734,9):41,(430,18):133,(90,13):77,(900,13):65,(9000,13):77,(90000,13):77,(450000,13):77,(90000,8):34,(450000,8):34,(9000,23):202,(90000,23):170,(450000,23):202}

def getOptimalSampleSize(N, r):
        if (N,r) in OPT_SAMPLE_SIZES:
            log( N, newline = False)
            log( r, newline = False)
            log( OPT_SAMPLE_SIZES[(N,r)], newline = False)
            return OPT_SAMPLE_SIZES[(N,r)]
        optNs = []
        with localcontext() as ctx:
            ctx.prec = 15
            for a in xrange(1,5):
                for k in xrange(1,5):
                    optN = 1
                    curMax = -1.
                    for n in xrange(2,N):
                        #h = math.log(float(N)/float(n))/math.log(float(r)) #typically too big, gives overflow
                        #val = (a*(n**k))**(2**h-1)
                        #val *= 1./(r**(h*k+2**h))
                        
                        h = math.log(float(N)/float(n))/math.log(float(r))
                        val = Decimal((a*(n**k)))**Decimal(2**h-1)
                        val *= Decimal(1./(r**(h*k+2**h)))
                                                
                        if val > curMax:
                            curMax = val
                            optN = n
                optNs.append(optN)
        hOptN = optNs[0]
        curMin = 10000000
        for n in optNs:
            val = math.log(float(N)/float(n)) % math.log(float(r))
            if val < curMin:
                curMin = val
                hOptN = n
        log( "(%d,%d):%d" %(N,r,hOptN))
        return hOptN

class RadonBoost(Learner):
    def __init__(self, method, minSampleSize = MINSAMPLESIZE, h=-1):
        self.identifier = "RadonBoost("+str(method)+")"
        self.minSampleSize = minSampleSize
        self.h = h
        self.method = method
    
    def determineSampleSize(self, X):
        instCount = X.shape[0]
        R = rc.getRadonNumber(X)
        self.minSampleSize = getOptimalSampleSize(instCount, R)            
            
    def train(self, X, y, bRegression = False):           
        self.bRegression = bRegression
        instCount = X.shape[0]
        self.method.train(X,y)
        w = self.method.getModel()
        R = rc.getRadonNumber(w) #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)
        h = self.h
        if h < 1:
            h = int(math.log(float(instCount) / float(self.minSampleSize),float(R)))        
        if instCount/(R**self.h) <= self.minSampleSize:            
            while instCount/(R**h) <= self.minSampleSize:
                h -= 1
            log( "Using height "+str(self.h)+" not possible, too few data points for min-sample-size "+str(self.minSampleSize)+". Using height "+str(h)+" instead.")
        
        baseSampleSize = int(instCount/(R**h))
        
        indices = np.arange(instCount)
        np.random.shuffle(indices)
        #sample_indices = self.getStratifiedSample(indices, y, int(R**h), baseSampleSize)
        sample_indices = np.array_split(indices, int(R**h))
        while any([all([x == y[sample][0] for x in y[sample]]) for sample in sample_indices]): #make this more efficient
            log( "Error: stratified sampling went wrong...retry")
            np.random.shuffle(indices)
            sample_indices = self.getStratifiedSample(indices, y, int(R**h), baseSampleSize)
        weights = []
        
        trainduration = 0.0        
        for sample in sample_indices:
            start = datetime.now()
            Xs = X[sample]
            ys = y[sample]
            self.method.train(Xs,ys)
            stop = datetime.now()
            trainduration += (stop - start).total_seconds()
            w = self.method.getModel()[0]
            weights.append(w)        
        trainduration /= float(len(sample_indices))   
             
        if len(weights) != R**h:
            log( "Something went wrong. Expected number of model: "+str(R**h)+" but "+str(len(weights))+" models have been created. ", newline = False)
            log( "(N:"+str(instCount)+" D:"+str(X.shape[1])+" h:"+str(h)+" sampleSize:"+str(baseSampleSize)+")")
        S = np.array(weights)
        start = datetime.now() 
        self.optimalModel = np.array([rc.getRadonPointHierarchical(S, h)]) #works like this only for binary classification and regression, where there is only one linear model per learner. For multiclas, something else must be done here.
        self.method.setModel(self.optimalModel)
        stop = datetime.now()
        c = float(len(sample_indices))
        overhead = (math.log(c)/math.log(float(R)))
        syncduration = ((stop - start).total_seconds() / c)*overhead
        return {'train':trainduration,'sync':syncduration}
        
    def predict(self, X):
        return self.method.predict(X)
    
    def getStratifiedSample(self, indices, y, noOfSamples, baseSampleSize):
        if self.bRegression:
            np.random.shuffle(indices)
            samples = [s for s in np.array_split(indices, noOfSamples)]
            return samples
        if len(np.unique(y)) == 2:
            return self.getStratifiedSampleBinary(indices, y, noOfSamples, baseSampleSize)
        else:
            return self.getStratifiedSampleMulticlass(indices, y, noOfSamples, baseSampleSize)

    def getStratifiedSampleMulticlass(self, indices, y, noOfSamples, baseSampleSize):
        classes = np.unique(y)
        samples = {}
        for c in classes:
            idx = [i for i in indices if y[i] == c]       
            sampleSize = int(float(len(idx)) / float(len(indices)) * baseSampleSize)      
            c_samples = np.split(idx, [i*sampleSize for i in xrange(1,noOfSamples)])
            samples[c] = c_samples
        sampleList = []
        for i in xrange(noOfSamples):
            s = np.array([]).astype(int)
            for c in classes:
                s = np.append(s, samples[c][i])
            sampleList.append(s.astype(int))
        return sampleList
  
    def getStratifiedSampleBinary(self, indices, y, noOfSamples, baseSampleSize):
        pos = [i for i in indices if y[i] == 1.0]
        neg = [i for i in indices if y[i] == 0.0]
        if len(pos)+len(neg) != len(indices):
            log( "Error: more classes than 1 and 0 for stratified binary sampling.")
        posSampleSize = int(float(len(pos)) / float(len(indices)) * baseSampleSize)
        negSampleSize = int(float(len(neg)) / float(len(indices)) * baseSampleSize)      
        pos_samples = np.split(pos, [i*posSampleSize for i in xrange(1,noOfSamples)])
        neg_samples = np.split(neg, [i*negSampleSize for i in xrange(1,noOfSamples)])
        if len(pos_samples) != len(neg_samples):
            log( "Error: wanted the same number of samples but got something different for positive and negative examples...")
        samples = []        
        for i in xrange(len(pos_samples)):
            samples.append(np.append(pos_samples[i], neg_samples[i]).astype(int))
        return samples

class MinSampleRadonBoost(Learner):
    def __init__(self, method, minSampleSize = 2, h=-1):
        self.identifier = "MinSampleRB("+str(method)+")"
        self.minSampleSize = minSampleSize
        self.h = h
        self.method = method
    
    def determineSampleSize(self, X):
        instCount = X.shape[0]
        R = rc.getRadonNumber(X)
        self.minSampleSize = getOptimalSampleSize(instCount, R)            
            
    def train(self, X, y, bRegression = False): #works currently only with binary classification
        instCount = X.shape[0]
        self.method.train(X,y)
        w = self.method.getModel()
        R = rc.getRadonNumber(w) #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)
        h = self.h
        if h < 1:
            h = int(math.log(float(instCount) / float(self.minSampleSize),float(R)))        
        if instCount/(R**self.h) <= self.minSampleSize:            
            while instCount/(R**h) <= self.minSampleSize:
                h -= 1
            log( "Using height "+str(self.h)+" not possible, too few data points for min-sample-size "+str(self.minSampleSize)+". Using height "+str(h)+" instead.")
        
        indices = np.arange(instCount)
        np.random.shuffle(indices)
        sample_indices = self.getMinimalSample(indices, y, int(R**h))
        baseSampleSize = len(sample_indices[0])
        while any([all([x == y[sample][0] for x in y[sample]]) for sample in sample_indices]): #make this more efficient
            log( "Error: stratified sampling went wrong...retry")
            np.random.shuffle(indices)
            sample_indices = self.getMinimalSample(indices, y, int(R**h))
        weights = []
        
        trainduration = 0.0        
        for sample in sample_indices:
            start = datetime.now()
            Xs = X[sample]
            ys = y[sample]
            self.method.train(Xs,ys)
            stop = datetime.now()
            trainduration += (stop - start).total_seconds()
            w = self.method.getModel()[0]
            weights.append(w)        
        trainduration /= float(len(sample_indices))   
             
        if len(weights) != R**h:
            log( "Something went wrong. Expected number of model: "+str(R**h)+" but "+str(len(weights))+" models have been created. ", newline = False)
            log( "(N:"+str(instCount)+" D:"+str(X.shape[1])+" h:"+str(h)+" sampleSize:"+str(baseSampleSize)+")")  
        S = np.array(weights)
        start = datetime.now() 
        self.optimalModel = np.array([rc.getRadonPointHierarchical(S, h)]) #works like this only for binary classification and regression, where there is only one linear model per learner. For multiclas, something else must be done here.
        self.method.setModel(self.optimalModel)
        stop = datetime.now()
        c = float(len(sample_indices))
        overhead = (math.log(c)/math.log(float(R)))
        syncduration = ((stop - start).total_seconds() / c)*overhead
        return {'train':trainduration,'sync':syncduration}
        
    def predict(self, X):
        return self.method.predict(X)
    
    def getMinimalSample(self, indices, y, noOfSamples):
        np.random.shuffle(indices)
        pos = [i for i in indices if y[i] == 1.0]
        neg = [i for i in indices if y[i] == 0.0]
        if len(pos)+len(neg) != len(indices):
            log( "Error: more classes than 1 and 0 for stratified binary sampling.")
        fracPos = float(len(pos)) / float(len(indices))
        fracNeg = float(len(neg)) / float(len(indices))
        posSampleSize = 1
        negSampleSize = 1
        if pos < neg:
            negSampleSize = int(fracNeg / fracPos)
            if negSampleSize < 1:
                negSampleSize = 1
        else:
            posSampleSize = int(fracPos / fracNeg)
            if posSampleSize < 1:
                posSampleSize = 1
        pos_samples = np.array_split(pos, [i*posSampleSize for i in xrange(1,noOfSamples)])
        neg_samples = np.array_split(neg, [i*negSampleSize for i in xrange(1,noOfSamples)])
        if len(pos_samples) != len(neg_samples):
            log( "Error: wanted the same number of samples but got something different for positive and negative examples...")
        samples = []        
        for i in xrange(len(pos_samples)):
            samples.append(np.append(pos_samples[i], neg_samples[i]).astype(int))
        return samples
    
class MaxSampleRadonBoost(RadonBoost):
    def __init__(self, method, minSampleSize = MINSAMPLESIZE, h=-1, k = 2):
        self.identifier = "MaxSampleRadonBoost("+str(method)+")"
        self.minSampleSize = minSampleSize
        self.h = h
        self.k = k
        self.method = method
            
    def train(self, X, y, bRegression = False):           
        self.bRegression = bRegression       
        instCount = X.shape[0]
        self.method.train(X,y)
        w = self.method.getModel()
        R = rc.getRadonNumber(w) #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)
        h = self.h
        if h < 1:
            h = int(math.log(float(instCount) / float(self.minSampleSize),float(R)))        
        if instCount/(R**self.h) <= self.minSampleSize:            
            while instCount/(R**h) <= self.minSampleSize:
                h -= 1        
        baseSampleSize = int(instCount/(R**h))
        
        indices = np.arange(instCount)        
        sample_indices = []
        for i in xrange(int(R**((self.k - 1)*h))):
            np.random.shuffle(indices)
            sample_indices += self.getStratifiedSampleBinary(indices, y, int(R**h), baseSampleSize)                        
        weights = []
        
        trainduration = 0.0         
        for sample in sample_indices:
            start = datetime.now()
            Xs = X[sample]
            ys = y[sample]
            self.method.train(Xs,ys)
            stop = datetime.now()
            trainduration += (stop - start).total_seconds()
            weights.append(self.method.getModel()[0])
        trainduration /= float(len(sample_indices))        
        
        if len(weights) != R**(self.k*h):
            log( "Something went wrong. Expected number of model: "+str(R**h)+" but "+str(len(weights))+" models have been created. ", newline = False)
            log( "(N:"+str(instCount)+" D:"+str(X.shape[1])+" h:"+str(h)+" sampleSize:"+str(baseSampleSize)+")")
        
        start = datetime.now()               
        S = np.zeros((len(weights),len(weights[0])))
        for i in xrange(len(weights)):
            for j in xrange(len(weights[i])):
                S[i][j] = weights[i][j]
        self.optimalModel = np.array([rc.getRadonPointHierarchical(S, self.k*h)]) #works like this only for binary classification and regression, where there is only one linear model per learner. For multiclas, something else must be done here.
        self.method.setModel(self.optimalModel)     
        stop = datetime.now()
        c = float(len(sample_indices))
        syncduration = ((stop - start).total_seconds() / c)*(math.log(c)/math.log(float(R))) 
        return {'train':trainduration,'sync':syncduration}
        
class AveragingBaseline(RadonBoost):
    def __init__(self, method, minSampleSize = MINSAMPLESIZE, h=-1, k = 2):
        self.identifier = "AveragingBaseline("+str(method)+")"
        self.minSampleSize = minSampleSize
        self.h = h
        self.k = k
        self.method = method
            
    def train(self, X, y, bRegression = False):           
        self.bRegression = bRegression        
        instCount = X.shape[0]
        self.method.train(X,y)
        w = self.method.getModel()
        R = rc.getRadonNumber(w) #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)
        h = self.h
        if h < 1:
            h = int(math.log(float(instCount) / float(self.minSampleSize),float(R)))        
        if instCount/(R**self.h) <= self.minSampleSize:            
            while instCount/(R**h) <= self.minSampleSize:
                h -= 1
            log( "Using height "+str(self.h)+" not possible, too few data points for min-sample-size "+str(self.minSampleSize)+". Using height "+str(h)+" instead.")
        
        baseSampleSize = int(instCount/(R**h))
        
        indices = np.arange(instCount)
        np.random.shuffle(indices)
        sample_indices = self.getStratifiedSample(indices, y, int(R**h), baseSampleSize)
        while any([all([x == y[sample][0] for x in y[sample]]) for sample in sample_indices]): #make this more efficient
            log( "Error: stratified sampling went wrong...retry")
            np.random.shuffle(indices)
            sample_indices = self.getStratifiedSample(indices, y, int(R**h), baseSampleSize)
            
        start = datetime.now()   
        weights = []
        for sample in sample_indices:
            Xs = X[sample]
            ys = y[sample]
            self.method.train(Xs,ys)
            weights.append(self.method.getModel()[0])
        stop = datetime.now()
        trainduration = (stop - start).total_seconds() / float(len(sample_indices))        
        
        if len(weights) != R**h:
            log( "Something went wrong. Expected number of model: "+str(R**h)+" but "+str(len(weights))+" models have been created. ", newline = False)
            log( "(N:"+str(instCount)+" D:"+str(X.shape[1])+" h:"+str(h)+" sampleSize:"+str(baseSampleSize)+")")
        
        start = datetime.now()                
        S = np.array(weights)
        self.optimalModel = np.array(self.getAverage(S)) #works like this only for binary classification and regression, where there is only one linear model per learner. For multiclas, something else must be done here.
        self.method.setModel(self.optimalModel)    
        stop = datetime.now()
        c = float(len(sample_indices))
        syncduration = ((stop - start).total_seconds() / c)*(math.log(c)/math.log(float(R)))
        return {'train':trainduration,'sync':syncduration}
        
    def getAverage(self, S):
        model = np.zeros(S.shape[1])
        for weight in S:
            model += weight
        model /= float(S.shape[0])        
        return model.reshape(1,model.shape[0])
    
class EfficientMiniBatchSGDBaseline(RadonBoost):
    def __init__(self, method, minSampleSize = MINSAMPLESIZE, h=-1, k = 2):
        self.identifier = "EfficientMiniBatchSGDBaseline"
        self.minSampleSize = minSampleSize
        self.h = h
        self.k = k
        self.method = method
            
    def train(self, X, y, bRegression = False):           
        self.bRegression = bRegression        
        instCount = X.shape[0]
        self.method.train(X,y)
        w = self.method.getModel()
        R = rc.getRadonNumber(w) #we need the Radon number of the model class, not of the data (it can be different, e.g., for multiclass)
        h = self.h
        if h < 1:
            h = int(math.log(float(instCount) / float(self.minSampleSize),float(R)))        
        if instCount/(R**self.h) <= self.minSampleSize:            
            while instCount/(R**h) <= self.minSampleSize:
                h -= 1
            log( "Using height "+str(self.h)+" not possible, too few data points for min-sample-size "+str(self.minSampleSize)+". Using height "+str(h)+" instead.")
        
        baseSampleSize = int(instCount/(R**h))
        
        indices = np.arange(instCount)
        np.random.shuffle(indices)
        sample_indices = self.getStratifiedSample(indices, y, int(R**h), baseSampleSize)
        while any([all([x == y[sample][0] for x in y[sample]]) for sample in sample_indices]): #make this more efficient
            log( "Error: stratified sampling went wrong...retry")
            np.random.shuffle(indices)
            sample_indices = self.getStratifiedSample(indices, y, int(R**h), baseSampleSize)
            
        start = datetime.now()   
        weights = []
        for sample in sample_indices:
            Xs = X[sample]
            ys = y[sample]
            self.method.train(Xs,ys)
            weights.append(self.method.getModel()[0])
        stop = datetime.now()
        trainduration = (stop - start).total_seconds() / float(len(sample_indices))        
        
        if len(weights) != R**h:
            log( "Something went wrong. Expected number of model: "+str(R**h)+" but "+str(len(weights))+" models have been created. ", newline = False)
            log( "(N:"+str(instCount)+" D:"+str(X.shape[1])+" h:"+str(h)+" sampleSize:"+str(baseSampleSize)+")")
        
        start = datetime.now()                
        S = np.array(weights)
        self.optimalModel = np.array(self.getAverage(S)) #works like this only for binary classification and regression, where there is only one linear model per learner. For multiclas, something else must be done here.
        self.method.setModel(self.optimalModel)    
        stop = datetime.now()
        c = float(len(sample_indices))
        syncduration = ((stop - start).total_seconds() / c)*(math.log(c)/math.log(float(R)))
        return {'train':trainduration,'sync':syncduration}
        
    def getAverage(self, S):
        model = np.zeros(S.shape[1])
        for weight in S:
            model += weight
        model /= float(S.shape[0])        
        return model.reshape(1,model.shape[0])
        
if __name__ == "__main__":
    R = RadonBoost(5)
    R.getOptimalSampleSize(100, 5)
            